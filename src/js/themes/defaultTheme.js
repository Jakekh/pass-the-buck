Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = defaultTheme 

//const _colors = require('material-ui/styles/colors')
//const _colorManipulator = require('material-ui/utils/colorManipulator')

const theme = {
  palette: {
    primary: {
      light: '#5ab2ff',
      main: '#3f7eb5',
      dark: '#2b5982',
      contrastText: '#fff',
    },
    secondary: {
      light: '#f3f3f3',
      main: '#333',
      dark: '#222',
      contrastText: '#fff',
    }
  }
}

function defaultTheme() {
   return theme
};