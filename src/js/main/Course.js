var React = require('react')

import * as CourseActions from '../actions/CourseActions'
import CourseStore from '../stores/CourseStore'

//import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
//import defaultTheme from '../themes/defaultTheme'
//import muiThemeable from 'material-ui/styles/muiThemeable'
//import getMuiTheme from 'material-ui/styles/getMuiTheme'

import LmsStore from '../stores/LmsStore'

import View from '../components/View'

export default class Course extends React.Component {
	constructor(){
		super()
		this.state = {
			view: CourseStore.returnCourseView(),
		}
	}
	componentWillMount() {
		CourseStore.on( "view_changed", this._returnCourseView )
		LmsStore.dataLoaded ? this._startCourse() : LmsStore.on( "lms_data_loaded", this._startCourse )
		LmsStore.on( "prompt_testout", this._startTestout )
	}
	_startTestout = () => {
		console.log("TEST OUT")
	}
	_returnCourseView = () => {
		//console.log("view_changed")
		this.setState({
			view: CourseStore.returnCourseView()
		})
	}

	_startCourse = () => {
		//console.log("Starting Course", LmsStore.returnLocation())
		CourseActions.createView(LmsStore.returnLocation())
	}

	render() {
		//const theme = defaultTheme()
		const{ view } = this.state
		//console.log("view: ", view)
		const styles = {
			chip: {
				borderRadius: '20px',
			    height: '40px',
			    transition: 'all .5s'
			},
		}
		return (
			<div>
				{ view ?
					<View  /  >
				:
					<div> No View </div>
				}
			</div>
		)
	}
}
