var React = require('react')

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core';

import defaultTheme from '../themes/defaultTheme'
//import muiThemeable from '@material-ui/core/styles/muiThemeable'
//import getMuiTheme from '@material-ui/core/styles/getMuiTheme'
import * as CourseActions from '../actions/CourseActions'
import CourseStore from '../stores/CourseStore'
import LmsStore from '../stores/LmsStore'
import MsgStore from '../stores/MsgStore'

import Message from '../components/messages/message'
import Modal from '../components/UI/Modal'
import Sidebar from '../components/UI/Sidebar'
import Toolbar from '../components/UI/Toolbar'
import Resources from '../components/UI/Resources'
import Glossary from '../components/UI/Glossary'
import CircularProgress  from '@material-ui/core/CircularProgress';

export default class Layout extends React.Component {
	constructor(){
		super()
		console.log(LmsStore.isReady)
		this.state = {
			lmsStoreReady: LmsStore.isReady ? true : false,
			courseStoreReady: CourseStore.isReady ? true : false,
			hasData: CourseStore.returnCourseData() ? true : false,
			course: CourseStore.returnCourseData(),
			menuOpen: true,
		}
	}
	toggleMenu = () => {
		this.setState({menuOpen: this.state.menuOpen ? false : true})
	}
	componentWillMount = () => {
		CourseStore.on("store_ready", this.updateReady.bind(this, "courseStore"))
		LmsStore.on("store_ready", this.updateReady.bind(this, "lmsStore"))
		LmsStore.on("prompt_testout", this.promptTestout.bind(this, "lmsStore"))
		CourseActions.startLmsStore()
		CourseActions.startCourseStore()
		this.awaitData()
	}
	promptTestout = (store) => {
		this.setState({
			lmsStoreReady: true
		}, () => {
			MsgStore.createMsg({
				type: "prompt",
				title: "Test Out",
				message: LmsStore.config.assessment.testOutMessage,
				submit: this.startTestout.bind(this)
			})
		})
	}
	startTestout = () => {
		CourseActions.startTestout()
	}
	updateReady = (store) => {
		store == "lmsStore" ?
			this.setState({
				lmsStoreReady: true
			})
		:
			this.setState({
				courseStoreReady: true
			})
	}
	awaitData = () => {
		let attempts = 0
		let that = this
		var apiInterval = setInterval(function(){
			attempts++
			if(CourseStore.returnCourseData()){
				console.log("HERES THE COURSE DATA", CourseStore.returnCourseData() )
				clearInterval(apiInterval)
				that.setState({course: CourseStore.returnCourseData(), hasData: true})
			}else{
				//FOR TESTING LET IT FAIL
				if(attempts > 40){
					clearInterval(apiInterval)
					that.setState({course: CourseStore.returnCourseData(), hasData: true})
				}
			}
		}, 200)

	}
	render() {
		console.log(this.state.lmsStoreReady)
		const styles = {
			courseTitle: {
				paddingLeft: "20px",
			},

			logo: {
				maxWidth: "140px",
				padding: "10px",
			},
			sidebarHolder: {
				width: this.state.menuOpen ? "300px" : "0px",
			},
			contentHolder: {
				transition: "paddingLeft, 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
				paddingLeft: this.state.menuOpen && window.innerWidth > 840 ? "300px" : "0px"
			},
			loader: {
			  	width: '100%',
			  	textAlign: 'center',
			  	marginTop: "400px",
		  	},
		}
		//console.log(defaultTheme)
		const theme = createMuiTheme(defaultTheme())
		const {course, menuOpen, hasData, lmsStoreReady, courseStoreReady} = this.state
		return (

			<MuiThemeProvider theme={theme} >
				{ hasData && lmsStoreReady && courseStoreReady ? (
				<div class="mdl-layout">
					{ course ?
						<div >

							<div style={styles.sidebarHolder}>
						   		<Sidebar menuOpen={menuOpen} toggleMenu={this.toggleMenu.bind(this)}/>
						   	</div>
						   	<div style={styles.contentHolder}>
						   		<Toolbar course={ course } menuOpen={menuOpen} toggleMenu={this.toggleMenu.bind(this)}/>
								<div class="layoutDiv" >
									{this.props.children}
								</div>
							</div>
						</div>

					:
						<p>Loading</p>
					}
					<Message />
					<Modal />
					<Resources />
					<Glossary />
				</div>
				) :	(<div style={styles.loader}><CircularProgress size={50} /></div> ) }

				</MuiThemeProvider>
		)
	}
}
