import { EventEmitter } from "events"

import $ from 'jquery'
import dispatcher from "../dispatcher"
import LmsStore from './LmsStore'

class CourseStore extends EventEmitter {
	constructor(){
		super()
		let themeFile = require('../themes/defaultTheme.js')
		this.configData = require('../../data/course-config.json')
		this.courseData = require('../../data/course-data.json')
		this.glossaryData = require('../../data/glossary.json')
		this.mode = "standard"
		this.theme = themeFile.default()
		this.questions = []
		this.displayWidth = $(window).width()
		this.checkBreakPoint()
		$(window).on('resize', () => {
			this.checkBreakPoint()
		})
	}

	listen(){
		console.log("LmsStore Listening...")
		!LmsStore ?
			setTimeout(this.listen, 5)
		:
			this.splitThresholds(this.courseData)
	}
	checkBreakPoint(){
		let width = $(window).width()
		let breakpoint = null
		width < 480 ?
			breakpoint = {
				name: 'xxsmall',
				index : 0
			}
		:
		width < 600 ?
			breakpoint = {
				name: 'xsmall',
				index : 1
			}
		:
		width < 840 ?
			breakpoint = {
				name: 'small',
				index : 2
			}
		:
		width < 960 ?
			breakpoint = {
				name: 'medium',
				index : 3
			}
		:
		width < 1280 ?
			breakpoint = {
				name: 'large',
				index : 4
			}
		:
		width >= 1280 ?
			breakpoint = {
				name: 'xlarge',
				index : 5
			}
		: null

		this.displayWidth = $(window).width()
		if( !this.breakpoint || this.breakpoint && this.breakpoint.name != breakpoint.name ){
			this.breakpoint = breakpoint
			this.emit('breakpoint_changed')
			//console.log("breakpoint_changed", this.displayWidth, breakpoint )
		}
	}
	splitThresholds(data){
		console.log("THS")
		/*Map through all modules*/
		this.moduleThresholds = []
		let that = this
		 _.map(this.courseData.modules, function(module, modkey){
			let sectionThresholds = []
			let tmpAry = []
			/*For each module, map through all sections  - CLEAN THIS UP*/
			 _.map(module.sections, function(section, seckey){
			 	let isThreshold = _.find(section, function(o) { return o.threshold  })
				if(isThreshold ){
					tmpAry.push(section)
					sectionThresholds.push(tmpAry)
					tmpAry = []
				}else{
					tmpAry.push(section)
				}
			})
			that.moduleThresholds.push(sectionThresholds)
		})
		console.log("CourseStore ready")
		this.isReady = true
		this.emit("store_ready")
	}
	addThreshold(){
		let that = this
		that.view = []
		let totalThresholds = 0
		let lSection = LmsStore.getProgress()[this.location[0]].indexOf(0)
		let lastThreshold = 0
		_.each( this.moduleThresholds[this.location[0]], function(threshold, index){
			totalThresholds += threshold.length
			that.view = that.view.concat(threshold)
			lastThreshold++
			if(lSection < totalThresholds ){
				return false;
			}
		})
		this.emit("view_changed")
	}
	testOut(){
		//Lets find the most appropriate Assessment section to test
		let sections = _.map(this.courseData.modules, (module, index) => {
			return _.filter(module.sections, function(s){return s[Object.keys(s)[0]].testOutAssessment == true})
		})
		console.log(sections)
		sections ?
			this.view = _.flatten(sections)
		: null //Should prompt testout err
		this.mode = "testout"
		this.emit("view_changed")
		this.emit("mode_changed")
	}
	exitTestout = () => {
		this.mode = "standard"
		LmsStore.progress = LmsStore.initProgress()
		this.loadModule([0,0])
		window.scrollTo(0, 0);
		this.emit("view_changed")
		this.emit("mode_changed")
	}
	loadModule(newLocation){
		if(newLocation){
			////console.log('newLocation: ', newLocation)
			this.location = newLocation
			LmsStore.setLocation(newLocation)
		}

		if(this.location){
			////console.log('location exists, location is: ', this.location)
			const that = this
			const progress = LmsStore.getProgress()[this.location[0]]

			//console.log(progress,LmsStore.getProgress())
			if(progress.indexOf(0) != -1) {
				console.log('progress: ', progress, "progress.indexOf(0) != -1 ", this.view)

				that.view = []
				let lSection = progress.indexOf(0)
				//console.log("lSection: ", lSection)
				let totalThresholds = 0
				_.each( this.moduleThresholds[this.location[0]], function(threshold, index){
					totalThresholds += threshold.length
					//console.log("threshold: ", totalThresholds, lSection, threshold)
					that.view = that.view.concat(threshold)
					if(lSection < totalThresholds ){
						//console.log("Dropping")
						return false;
					}
				})
			}else {
				console.log('progress: ', progress, "else progress.indexOf(0) != -1 ", this.view)
				that.view = []
				_.map(this.moduleThresholds[this.location[0]], function(threshold, index){
					that.view = that.view.concat(threshold)
				})
			}
		}else{
			this.location = [0,0]
			LmsStore.setLocation([0,0])
			this.view = this.moduleThresholds[this.location[0]][this.location[1]]
		}
		console.log("NEW VIEW: ", this.view)
	}
	returnCourseData(){
		/*Return entire course object*/
		return this.courseData
	}
	returnCourseView(){
		/*Return only current view data - based on progress*/
		return this.view
	}
	returnLocation(){
		/*Return only current view data - based on progress*/
		return this.location
	}
	returnThresholds() {
		return this.moduleThresholds
	}

  closeWindow() {
  	let win = window
      while (win != win.parent)
      win = win.parent
      win.close()
  }

	showResources = () => {
		this.emit('show_resources')
	}

	showGlossary = () => {
		this.emit('show_glossary')
	}

	returnDisplayWidth = () => {
		return $(window).width()
	}
	addQuestionsData(obj){
		console.log("ADDING INTERACTION: ", obj)
		const replaceIndex = _.findIndex(this.questions, {id: obj.id})
		replaceIndex != -1 ? this.questions[replaceIndex] = obj : this.questions.push(obj)
	}
	generateQuestions = ( id, pools ) => {
		let that = this

		let questionAry = []

		_.map(pools, function(pool, key){
			let poolSample = pool.questions

			if(that.configData.randomizeQuestions && that.configData.pooling){
				_.shuffle(pool.questions)
				poolSample = _.sampleSize(pool.questions, pool.pull )
			} else if(that.configData.randomizeQuestions){
				poolSample = _.shuffle(pool.questions)
			} 
			
			questionAry.push(poolSample)
		})

		questionAry = _.flattenDeep(questionAry)

		this.addQuestionsData({id: id , questions: questionAry})
		
		//console.log("Questions Created", this.questions)
		this.emit('questions_generated')
	}
	returnQuestions = (key) => {
		console.log(this.questions, key)
		console.log(_.find(this.questions, function(value) { return value.id == key }))
		return _.find(this.questions, function(value) { return value.id == key })
	}

	_findTerm = (term) => {
		const item = _.find(this.glossaryData, {'term': term})

		if(item != undefined) {
			this.infolinkItem = item
		}else{
			const altItem = _.find(this.glossaryData, {'alt': term})
			if(altItem != undefined) {
				this.infolinkItem = altItem
			}
		}

		this.emit("show_infolink")
	}

	handleActions(action) {
		switch(action.type) {
			case "UPDATE_PROGRESS" : {
				LmsStore.setProgress(action.location)
				break
			}
			case "GETTING_VIEW": {
				LmsStore.setProgress(action.location)
				this.loadModule(action.location)
				this.emit("view_changed")
				break
			}
			case "LOAD_NEW_MODULE": {
				this.loadModule([action.modKey, 0])
				LmsStore.setProgress(this.location)
				$(window).scrollTop(0)
				this.emit("view_changed")
				break
			}
			case "NEXT_THRESHOLD": {

				let module = this.location[0]
				let section  = this.location[1]
				//console.log("GETTING NEXT THRESHOLD: ", module, section)
				if(this.moduleThresholds[module][section+1]){
					//console.log("GETTING TH: STORE")
					this.location[1] = this.location[1]+1
					this.addThreshold()
					//this.emit("view_changed")
				}else{
					//console.log("All Thresholds loaded...")
				}
				break
			}
			case "SHOW_GLOSSARY_TERM": {
				this.infolinkTerm = _.find(this.glossaryData, (item) => {
					console.log(item.term, action.term)
					return item.term == action.term
				})
				console.log(this.infolinkTerm)
				this.emit("show_infolink")
				break
			}
			case "SAVE_ASSESSMENT_REM": {
				this.assessmentRemediation = action.rem
				action.rem.length > 0 && this.emit("assessment_review_change")
				break
			}
			case "START_COURSE_STORE": {
				this.listen()
				break
			}
			case "START_TESTOUT": {
				//change view to assessment, load and launch
				this.testOut()
				console.log("TESTOUT")
				break
			}
			case "EXIT_TESTOUT": {
				//change view to assessment, load and launch
				this.exitTestout()
				break
			}
		}
	}
}

const courseStore = new CourseStore

dispatcher.register(courseStore.handleActions.bind(courseStore))

export default courseStore
