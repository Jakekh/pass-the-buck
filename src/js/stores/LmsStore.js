import { EventEmitter } from "events"

import $ from 'jquery'
import dispatcher from "../dispatcher"
import CourseStore from './CourseStore'

import { SCORM } from 'pipwerks-scorm-api-wrapper'
import AICC from '../AICCwrapper'

class LmsStore extends EventEmitter {
	constructor(){
		super()
		this.config = require('../../data/course-config.json')
	}

	listen(){
		!CourseStore ?
			setTimeout(this.listen, 200)
		:
			this.checkTracking()
	}

	returnLMSScore(){
		let score
		if(SCORM.init()) {
			score = this.version == '1.2' ? SCORM.get('cmi.core.score.raw') : SCORM.get('cmi.score.raw')
		}else {
	    	if(AICC.getUrlVar('aicc_sid') != 'null') {
	    		AICC.getAICC(data => score = data.score)
	    	}else {
				score = null
	    	}
		}
		return score
	}

	checkTracking() {
		console.log("CHECKING TRACKING")
		if(SCORM.init()) {
			this.scormSetup()
		}else {
	    	if(AICC.getUrlVar('aicc_sid') != 'null') {
	    		this.aiccSetup()
	    	}else {
				this.type = null
				this.suspendData = []
				this.interactionData = []
				this.progress = []
				this.moduleProgress = []
				this.score = null
				this.status = null
				this.location = [0,0]
				this.initCourse()
				//this.tryInitProgress()
	    	}
		}
	}
	aiccSetup() {
		const that = this
		AICC.getAICC(function(data) {
			that.initTime = new Date()
			that.type = 'AICC'
			if(data.progress == undefined) {
				that.suspendData = []
				that.interactionData = []
				that.moduleProgress = []
				that.progress = []
				//that.tryInitProgress()
			}else {
				that.suspendData = JSON.parse(data.progress)
				const iData = _.find(that.suspendData, 'i')
				const pData = _.find(that.suspendData, 'p')
				that.interactionData = iData != undefined ? iData.i : []
				that.progress = pData != undefined ? pData.p : []
				that.moduleProgress = []
				if(pData != undefined ){
					_.map(that.progress, (value, modkey) => { value.indexOf(0) == -1 ? that.moduleProgress.push(1) : that.moduleProgress.push(0)})
					that.emit("lms_data_loaded")
					that.dataLoaded = true
				}
				// else{
				// 	that.tryInitProgress()
				// }
			}
			that.score = data.score
			that.status = data.lesson_status
			data.lesson_location.length > 1 ?
				that.location = data.lesson_location.split(',').map(Number)
			:
				that.location = [0,0]
				this.initCourse()
			// route through this.initCourse() that.emit("lms_data_loaded")
		})
	}

	scormSetup(){
		this.type = 'SCORM'
		this.initTime = new Date()
		this.version = SCORM.version
		console.log("SUSPEND", SCORM.get('cmi.suspend_data'))
		SCORM.get('cmi.suspend_data').length > 0 ?
			this.suspendData = JSON.parse(SCORM.get('cmi.suspend_data'))
		:
			this.suspendData = []
		const iData = _.find(this.suspendData, 'i')
		const pData = _.find(this.suspendData, 'p')
		this.interactionData = iData != undefined ? iData.i : []
		this.progress = pData != undefined ? pData.p : []
		this.moduleProgress = []
		if(pData != undefined ){
			_.map(this.progress, (value, modkey) => { value.indexOf(0) == -1 ? this.moduleProgress.push(1) : this.moduleProgress.push(0) })
			this.emit("lms_data_loaded")
			this.dataLoaded = true
		}
		// else{
		// 	this.tryInitProgress()
		// }
		this.score = this.version == '1.2' ? SCORM.get('cmi.core.score.raw') : SCORM.get('cmi.score.raw')
		this.status = this.version == '1.2' ? SCORM.get('cmi.core.lesson_status') : SCORM.get('cmi.completion_status')
		this.location = this.version == '1.2' ? SCORM.get('cmi.core.lesson_location') : SCORM.get('cmi.location')
		this.location.length > 0 ?
			this.location = this.location.split(',').map(Number)
		:
			this.location = [0,0]
		this.initCourse()
	}
	initCourse() {
		console.log("InitCourse", CourseStore)
		//get progress, apply config, emit when done.
		this.progress = this.initProgress()
		let direction = this.checkForRedirects(this.progress)
		this.ready(direction)
	}
	ready(msg){
		console.log("LMS Store Ready")
		this.emit(msg)
		this.isReady = true
	}
	checkForRedirects() {
		let assessmentConfig = this.config.assessment
		console.log(this.progress)

		//allow student to testout of course?
		let emitCall = assessmentConfig.hasAssessment && assessmentConfig.allowCourseTestOut ?
			 SCORM.get('cmi.suspend_data').length > 0 ? "store_ready" : "prompt_testout"
		: null
		return emitCall
	}
	setInteractionData(data) {
		const interactions = {i: data}
		const replaceIndex = _.findIndex(this.suspendData, 'i')
		replaceIndex != -1 ? this.suspendData[replaceIndex] = interactions : this.suspendData.push(interactions)
		console.log("SUSPEND DATA: ", this.suspendData)
	}

	setProgressData(data) {
		const progress = {p: data}
		const replaceIndex = _.findIndex(this.suspendData, 'p')
		replaceIndex != -1 ? this.suspendData[replaceIndex] = progress : this.suspendData.push(progress)
		this.checkComplete()
	}

	saveSuspendData(cond = null) {
		const allSuspendData = JSON.stringify(this.suspendData)
		if(this.type == 'SCORM' && allSuspendData != SCORM.get('cmi.suspend_data')  ) {
			SCORM.set('cmi.suspend_data', allSuspendData)
			SCORM.save()
			if(cond == 'exit'){
				CourseStore.closeWindow()
			}
		}else if(this.type == 'AICC') {
			AICC.setAICC(allSuspendData, cond)
		}else{
			//console.log('you are running locally, not on LMS')
			if(cond == 'exit'){
				CourseStore.closeWindow()
			}
		}
		//console.log("NEW PROGRESS", this.progress)
		this.emit("user_progress_updated")
	}

	checkComplete() {
		const tmpProgressString = JSON.stringify(this.progress)
		const courseData = CourseStore.returnCourseData()
		let scoredAssessment = null
		_.map(courseData.modules, (value, key) => {
			_.map(value.sections, (sectionValue, key) => {
				_.map(sectionValue, (childrenValue, key) => {
					_.find(childrenValue.children, (obj) => {
						obj.Assessment ? obj.Assessment.scored ? scoredAssessment = obj.Assessment : scoredAssessment = null : null
					})
				})
			})
		})
		if(this.type != null) {
			if(tmpProgressString.search('0') == -1 || CourseStore.mode == "testout") {
				if(scoredAssessment != null && this.score >= scoredAssessment.masteryScore || scoredAssessment == null) {
					this.status = 'completed'
					SCORM.set('cmi.core.lesson_status', "completed")
					SCORM.save()
				}else {
					this.status = 'incomplete'
					SCORM.set('cmi.core.lesson_status', "incomplete")
					SCORM.save()
				}
			}
		}
		this.saveSuspendData()
	}

	// -- interaction functions --

	returnAllInteractionData(){
		return this.interactionData
	}

	returnInteractionData(key){
		console.log("INTERACTION DATA: ", this.interactionData, key)
		return _.find(this.interactionData, function(value) { return value.id == key })
	}

	addInteractionData(obj){
		console.log("ADDING INTERACTION: ", obj)
		const replaceIndex = _.findIndex(this.interactionData, {id: obj.id})
		replaceIndex != -1 ? this.interactionData[replaceIndex] = obj : this.interactionData.push(obj)
		this.setInteractionData(this.interactionData)
	}

	// -- progress functions --

	initProgress = () => {
		const that = this
		const data = CourseStore.returnCourseData()
		let progData = []

		 _.map(data.modules, function(module, modkey){
		 	that.moduleProgress.push(0)
			let sectionProgress = []
			//let tmpAry = []
			 _.map(module.sections, function(section, seckey){
				//tmpAry.push(section)
				sectionProgress.push(0)
				//tmpAry = []
			})
			progData.push(sectionProgress)
		})

		//Single module, maybe ?
		/*let progressMap = []
		_.map(data, function(value, modkey){
			that.moduleProgress.push(0)
			let thresAry = []
			_.map(value, function(value, threskey){
				console.log(value)
				thresAry.push(0) // 0 is incomplete per threshold
			})
			progressMap.push(thresAry)
		})*/
		//this.emit("lms_data_loaded")
		this.dataLoaded = true
		return progData
	}

	setProgress = (location) => {
		console.log("Saving Location: ", location)
		/*if(this.type == 'SCORM') {
			SCORM.set('cmi.core.lesson_location', location.toString())
		}*/
		//console.log("LMS STORE: Checking Progress: ", location, this.progress[location[0]][location[1]] )
		if(this.progress[location[0]][location[1]] != 1) {
			this.progress[location[0]][location[1]] = 1
			if(this.progress[location[0]].indexOf(0) == -1){
				this.setModuleCopmlete(location[0])
			}
			this.setProgressData(this.progress)
		}
	}

	setModuleCopmlete(module){
		if(this.moduleProgress[module] != 1) {
			this.moduleProgress[module] = 1
			this.emit("module_completed")
			this.saveSuspendData()
		}
	}

	getModuleProgress() {
		////console.log(this)
		return this.moduleProgress
	}

	getProgress() {
		return this.progress
	}

	setScore(score) {
		this.score = score
		SCORM.set('cmi.core.score.raw', score)
		SCORM.save()
		this.checkComplete()
	}

	getScore() {
		return this.score
	}

	setStatus(status) {
		this.status = status
	}

	getStatus() {
		return this.status
	}

	setLocation(location) {
		this.location = location
	}

	returnLocation() {
		return this.location
	}

	// -- LMS utilities --

	saveSCORMinteraction(obj){
		if(this.type == "SCORM" && this.version == "1.2" ){
			var cmiInteractionCount = SCORM.get('cmi.interactions._count')
			//cmiObjectivesCount = SCORM.get('cmi.interactions.' + cmiInteractionCount + '.objectives._count');

			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.id', obj.id );
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.type', obj.type );

			/*if(cmiObjectivesCount == '' || cmiObjectivesCount == undefined) {
				cmiObjectivesCount = 0;
			}*/

			//SCORM.set('cmi.interactions.' + cmiInteractionCount + '.objectives.' + cmiObjectivesCount + '.id', obj.objId );
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.time', obj.timestamp.toString() )
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.student_response', obj.learner_response.toString())
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.weighting', obj.weight )
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.result', obj.result )
			SCORM.set('cmi.interactions.' + cmiInteractionCount + '.latency', obj.latency )
			//SCORM.set('cmi.interactions.' + cmiInteractionCount + '.description', obj.description );

			_.map(obj.correct_responses, function(value, index) {
				SCORM.set('cmi.interactions.' + cmiInteractionCount + '.correct_responses.' + index + '.pattern', value)
			})
		}
	}

	returnFormattedTime(timestamp){
		if(this.type == "SCORM" && this.version == "1.2" ){
			return this.MillisecondsToCMIDuration(timestamp)

		}else if(this.type == "SCORM" && this.version == "2004"){
				return this.centisecsToISODuration(Math.floor(timestamp/10))
		}else{
			//console.log("Tracking Type Fail: ", this.type, timestamp)
		}
	}

	centisecsToISODuration(n) {
	    // Note: SCORM and IEEE 1484.11.1 require centisec precision
	    // Months calculated by approximation based on average number
	    // of days over 4 years (365*4+1), not counting the extra day
	    // every 1000 years. If a reference date was available,
	    // the calculation could be more precise, but becomes complex,
	    // since the exact result depends on where the reference date
	    // falls within the period (e.g. beginning, end or ???)
	    // 1 year ~ (365*4+1)/4*60*60*24*100 = 3155760000 centiseconds
	    // 1 month ~ (365*4+1)/48*60*60*24*100 = 262980000 centiseconds
	    // 1 day = 8640000 centiseconds
	    // 1 hour = 360000 centiseconds
	    // 1 minute = 6000 centiseconds

	    n = Math.max(n,0); // there is no such thing as a negative
	    //duration
	    var str = "P";
	    var nCs = n;
	    // Next set of operations uses whole seconds
	    var nY = Math.floor(nCs / 3155760000);
	    nCs -= nY * 3155760000;
	    var nM = Math.floor(nCs / 262980000);
	    nCs -= nM * 262980000;
	    var nD = Math.floor(nCs / 8640000);
	    nCs -= nD * 8640000;
	    var nH = Math.floor(nCs / 360000);
	    nCs -= nH * 360000;
	    var nMin = Math.floor(nCs /6000);
	    nCs -= nMin * 6000
	    // Now we can construct string
	    if (nY > 0) str += nY + "Y";
	    if (nM > 0) str += nM + "M";
	    if (nD > 0) str += nD + "D";
	    if ((nH > 0) || (nMin > 0) || (nCs > 0)) {
	        str += "T";
	        if (nH > 0) str += nH + "H";
	        if (nMin > 0) str += nMin + "M";
	                if (nCs > 0) str += (nCs / 100) + "S";
	    }
	    if (str == "P") str = "PT0H0M0S";
	      // technically PT0S should do but SCORM test suite assumes longer form.
	    return str;
	}


	MillisecondsToCMIDuration(n) {
		//Convert duration from milliseconds to 0000:00:00.00 format
	    var hms = "";
	    var dtm = new Date();   dtm.setTime(n);
	    var h = "000" + Math.floor(n / 3600000);
	    var m = "0" + dtm.getMinutes();
	    var s = "0" + dtm.getSeconds();
	    var cs = "0" + Math.round(dtm.getMilliseconds() / 10);
	    hms = h.substr(h.length-4)+":"+m.substr(m.length-2)+":";
	    hms += s.substr(s.length-2);//+"."+cs.substr(cs.length-2);
	    return hms
	}

	setSessionTime() {
		var now = new Date(),
			sessionTime = now.getTime() - this.initTime.getTime()// -- initTime is a pipeworks variable

		if(this.type == 'SCORM' ){
			if(this.version == '2004'){
				return SCORM.set("cmi.session_time", this.centisecsToISODuration(Math.floor(sessionTime/10)))
			}
			if(this.version == '1.2'){
				return SCORM.set("cmi.core.session_time", this.MillisecondsToCMIDuration(sessionTime))
			}
		}else if(this.type == 'AICC'){
			return this.MillisecondsToCMIDuration(sessionTime)
		}
	}

	handleActions(action) {
		switch(action.type) {
			case "SAVE_INTERACTION_STATE": {
				this.addInteractionData(action.obj)
				break
			}
			case "SETTING_USER_PROGRESS": {
				this.emit("user_progress_updated")
				break
			}
			case "SET_MODULE_COMPLETE": {
				//console.log("MOD COMPLTE")
				this.setModuleCopmlete(action.module)
				break
			}
			case "START_LMS_STORE": {
				this.listen()
				break
			}
		}
	}
}

const lmsStore = new LmsStore

dispatcher.register(lmsStore.handleActions.bind(lmsStore))

export default lmsStore
