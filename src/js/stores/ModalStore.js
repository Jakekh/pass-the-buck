import { EventEmitter } from "events"

// ModalStore.createModal({title: "", content: [])// -- use
class ModalStore extends EventEmitter {
	constructor() {
		super()
		this.content = { title: "", content: [] }
		this.onCloseEvent = null
	}

	fireEvent(e){
		this.emit(e)
	}

	createModal({ content, onCloseEvent }) {
		this.content = content
		onCloseEvent ? this.onCloseEvent = onCloseEvent : null
		console.log(this)
		this.emit("modal-opened")
	}
}

const modalStore = new ModalStore

export default modalStore