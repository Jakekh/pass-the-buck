import firebase from 'firebase'
import _ from 'lodash'

import dispatcher from '../dispatcher'

export function getCourseData(data) {
	dispatcher.dispatch({
		type: 'GETTING_COURSE',
		course: data
	})
}
export function createView(location) {
	dispatcher.dispatch({
		type: 'GETTING_VIEW',
		location: location
	})
}
export function loadModule(modKey) {
	dispatcher.dispatch({
		type: 'LOAD_NEW_MODULE',
		modKey: modKey
	})
}
export function loadNextThreshold() {
	dispatcher.dispatch({
		type: 'NEXT_THRESHOLD'
	})
}
export function saveInteraction(interactionObj) {
	dispatcher.dispatch({
		type: 'SAVE_INTERACTION_STATE',
		obj: interactionObj
	})
}
export function saveAssessmentRem(remed) {
	dispatcher.dispatch({
		type: 'SAVE_ASSESSMENT_REM',
		rem: remed
	})
}
export function startLmsStore() {
	dispatcher.dispatch({
		type: 'START_LMS_STORE',
	})
}

export function startCourseStore() {
	dispatcher.dispatch({
		type: 'START_COURSE_STORE',
	})
}

export function updateProgress(loc) {
	//console.log("PROGRESS: ", loc)
	dispatcher.dispatch({
		type: 'UPDATE_PROGRESS',
		location: loc
	})
}
export function infolink(term) {
	dispatcher.dispatch({
		type: 'SHOW_GLOSSARY_TERM',
		term: term
	})
}
export function startTestout() {
	dispatcher.dispatch({
		type: 'START_TESTOUT',
	})
}
export function exitTestout() {
	dispatcher.dispatch({
		type: 'EXIT_TESTOUT',
	})
}
