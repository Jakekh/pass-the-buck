var React = require('react')

import Snackbar from '@material-ui/core/Snackbar'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import ReactHtmlParser from 'react-html-parser'

import MsgStore from '../../stores/MsgStore'

export default class Msg extends React.Component {

	constructor(){
		super()
		this.state = {
			open: false,
			msg: {
				title: '',
				type: '',
				message: ''
			},
			timeout: 1500
		}
	}

	componentWillMount = () => {
		MsgStore.on('change', () => {
			this.setState({open: true, msg: MsgStore.getMessage()})
		})
	}

	componentWillUnmount = () => {
		MsgStore.removeAllListeners()
	}

	handleClose = () => {
	    this.setState({open: false})
	}

	handleSubmit = () => {
		const { submit } = this.state.msg
		this.handleClose()
		setTimeout(() => submit(), 150)
	}

	handleCustomAction = (action) => {
		// -- if no action then its a close btn
		action == null ? this.handleClose() : setTimeout(() => action(), 150)
	}

	render() {
		const { open, msg, timeout } = this.state
		let actions
		const that = this
		console.log(msg)
	 	if(msg.actions){
	 		actions = msg.actions
	 	}else if(msg.customActions){
	 		actions = _.map(msg.customActions, (value, key) => {
	 			return <Button key={ key } primary={ true } onClick={ that.handleCustomAction.bind(that, value.action) }>{ value.label }</Button>
	 		})
	 	}else{
			actions = msg.type == 'popup' ? [ <Button primary={ true } onClick={ this.handleClose } >close</Button> ] : [
		      	<Button primary={ true } onClick={ this.handleSubmit }>yes</Button>,
		      	<Button primary={ true } onClick={ this.handleClose }>no</Button>,
		    ]
		}

		let message
		const style = {
			content: {
				width: '400px',
				maxWidth: 'none',
			   	marginTop: '-7%',
			},
			content_wide: {
				width: '100%',
				maxWidth: '800px',
			},
			content_wider: {
				width: '100%',
				maxWidth: '1000px',
			},
			snackBar: {
				height: "auto",
				lineHeight: "20px",
				padding: "15px",
			}
		}
		if(open){
			if(msg.type == 'prompt' || msg.type == 'popup' || msg.type == 'popup-unparsed' || msg.type == 'help'){
				message = 	<Dialog
					          open={ open }
					          onClose={ this.handleClose }
					          aria-labelledby="alert-dialog-title"
					          aria-describedby="alert-dialog-description"
					        >
					          <DialogTitle id="alert-dialog-title">{ msg.title }</DialogTitle>
					          <DialogContent>
					            <DialogContentText id="alert-dialog-description">
					              { msg.type == 'help' || msg.type == "popup-unparsed" ? msg.message : ReactHtmlParser(msg.message) }
					            </DialogContentText>
					          </DialogContent>
					          <DialogActions>
					            { actions }
					          </DialogActions>
					        </Dialog>
			}else{
				message = <Snackbar open={ open } message={ ReactHtmlParser(msg.message) } autoHideDuration={ timeout } bodyStyle={style.snackBar} onRequestClose={ this.handleClose } />
			}
		}else{
			message = null
		}

		return (
			<div>{ message }</div>
		)
	}
}
