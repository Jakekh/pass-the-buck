const React = require('react')
import Icon from '@material-ui/core/Icon'
import { withTheme } from '@material-ui/core/styles';

class InteractionBumper extends React.Component {
	render() {
		const { style, text } = this.props
		const styles = {
			directions: {
				width: "100%",
				textAlign: "center",
				color: "#0d1017",
				background: "#fff",
				borderTop: "1px solid #c7c9d1",
				font: "600 1.5rem/3 'Teko', Arial, sans-serif",
			    paddingTop: ".3rem",
			    letterSpacing: ".15rem",
			    textTransform: "uppercase"
			},
			dirIcon: {
				color: "#ccc",
			},
		}
		return (
			<div class="continueDirections" style={ style ? { ...styles.directions, ...style } : styles.directions }>
				<span>{ text }</span>
			</div>
			)	
	}
}

export default withTheme()(InteractionBumper);
