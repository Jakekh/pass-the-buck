var React = require('react')
import $ from "jquery"

import InformationalPane from "../contentDisplay/InformationalPane"
import ClickandDiscover from "../contentDisplay/ClickandDiscover"
import DragNDrop from "../contentDisplay/DragNDrop"
import Assessment from "../contentDisplay/Assessment"
import CaseStudies from "../contentDisplay/CaseStudies"
import Brochure from "../contentDisplay/Brochure"
import Captivate from "../contentDisplay/Captivate"
import Question from "../contentDisplay/Question"
import Simulation from "../contentDisplay/Simulation"
import Carousel from "../contentDisplay/Carousel"
import InteractionBumper from './InteractionBumper'
import NarrationPane from "../contentDisplay/NarrationPane"
import Animation from "../contentDisplay/Animation"

import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'


import { withTheme } from '@material-ui/core/styles';

var VisibilitySensor = require('react-visibility-sensor');

class Section extends React.Component {
	constructor(){
		super()
		this.state = {
			sensorTriggered: false,
			directionsVisible: true
		}
	}
	getNextThreshold = () => {
		console.log("SECTION: ", this.props.sectionId)
		this.setState({ directionsVisible: false })
	 	let progressID = this.props.sectionId.split("_")
	 	progressID[0] = parseInt(progressID[0])
	 	progressID[1] = parseInt(progressID[1])
	 	CourseActions.updateProgress(progressID)
		CourseActions.loadNextThreshold()
	}
	componentDidUpdate(prevProps, prevState) {
		if(prevState.sensorTriggered == true) {
		 	this.setState({
		 		sensorTriggered: false
		 	})
		}
	}
	triggerSensor = (isVisible) => {
		 if(isVisible && !this.state.sensorTriggered){
			console.log("SECTION: ", this.props.sectionId)
		 	let progressID = this.props.sectionId.split("_")
			if(progressID[0] == "carousel"){
				progressID.shift()
				while (progressID.length > 2){
					progressID.pop()
					console.log("NEW: ", progressID)
				}
			}
		 	progressID[0] = parseInt(progressID[0])
		 	progressID[1] = parseInt(progressID[1])
		 	//console.log('Element is now %s', isVisible ? 'visible' : 'hidden', progressID);
		 	this.props.threshold === true ? this.getNextThreshold() : CourseActions.updateProgress(progressID)
		 	this.setState({
		 		sensorTriggered: true
		 	})
		 }
	}
	render() {
		const { sectionId, title, children, subTitle, shadowed, hideTitle, style } = this.props

		let that = this
		const styles = {
			section: {
				padding: "0px",
				//minHeight: "100vh"
			},
			shadowedSection: {
				//minHeight: "100vh",
			    boxShadow: "none",
			    background: this.props.theme.palette.primary.main,
			    padding: "0px",
			    color: this.props.theme.palette.primary.contrastText,
			    borderBottom: `3px solid ${this.props.theme.palette.primary.dark}`
			},
			subTitle: {

			}
		}

		const components = {
		    InformationalPane: InformationalPane,
		    ClickandDiscover: ClickandDiscover,
		    DragNDrop: DragNDrop,
		    Assessment: Assessment,
		    CaseStudies: CaseStudies,
		    Brochure: Brochure,
		    Captivate: Captivate,
		    Question: Question,
		    Carousel: Carousel,
		    NarrationPane: NarrationPane,
		    Simulation: Simulation,
		    Animation: Animation
		};

		const sectionName = title.replace(/\W+/g, '-').toLowerCase()

		let childrenIndex = 0
		const sectionComponents = _.map( children, function(value, childKey){
			childrenIndex ++
			let subComps =_.map( value, function(component, compKey){
				const ComponentType = components[compKey]
	    		return <ComponentType muiTheme={that.props.muiTheme} { ...component }
    				threshold={that.props.threshold ? that.props.threshold : null}
    				getNextThreshold={that.props.threshold ? that.getNextThreshold.bind(this) : null}
    				key={ `${compKey}_${childrenIndex}` }
    				componentId={`${sectionId}_${childKey}`} parentId={ `${sectionName}_${sectionId}_${childKey}` } shadowed={shadowed} />
	    		})
			return subComps
		})
		return (
				!this.props.threshold ?
				<VisibilitySensor partialVisibility={'bottom'} delayedCall={true} onChange={this.triggerSensor} scrollDelay={4000}>
					<section id={sectionName} style={shadowed ? style ? { ...styles.shadowedSection, ...style } : styles.shadowedSection : style ? { ...styles.section, ...style } : styles.section}>
						{ hideTitle ? null : <div class="container mdl-grid">
							<div class="mdl-cell mdl-cell--12-col">
								{ title ? <h2 class="section-title" style={this.props.titleStyle ? this.props.titleStyle : null }>{title}</h2> : null }
								{ subTitle ? <h3>{subTitle}</h3> : null }
							</div>
						</div> }
						{ this.props.container ? <div class="container mdl-grid">{ sectionComponents }</div> : sectionComponents }
						{ this.props.bottomCaret ? <div class="section-caret"></div> : null }
					</section>
				</VisibilitySensor>
			:
				<section id={sectionName} style={shadowed ? style ? { ...styles.shadowedSection, ...style } : styles.shadowedSection : style ? { ...styles.section, ...style } : styles.section}>
					{ hideTitle ? null : <div class="container mdl-grid">
							<div class="mdl-cell mdl-cell--12-col">
								{ title ? <h2 class="section-title" style={this.props.titleStyle ? this.props.titleStyle : null }>{title}</h2> : null }
								{ subTitle ? <h3>{subTitle}</h3> : null }
							</div>
						</div> }
					{ this.props.container ? <div class="container mdl-grid">{ sectionComponents }</div> : sectionComponents }
					{ this.props.thresholdDirections && this.state.directionsVisible ? <InteractionBumper style={ this.props.thresholdDirectionsStyle ? this.props.thresholdDirectionsStyle : {background: "#fff", color: "#0d1017"} } icon={ false } text={ this.props.thresholdDirections } /> : null }
					{ this.props.bottomCaret ? <div class="section-caret"></div> : null }
				</section>

		)
	}
}

export default withTheme()(Section);
