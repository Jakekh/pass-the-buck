var React = require('react')
var videojs = require('video.js')
import 'animation.gsap'
import 'debug.addIndicators'
import 'TextPlugin'
var ScrollMagic = require('scrollmagic')
import {TweenMax, TimelineLite, Animation} from "gsap";

import ModuleStats from  './ModuleStats'

export default class Banner extends React.Component {
	constructor(){
		super()

		this.state = {
			videoVisible: false
		}
	}
	loadVideo = () => {
		this.setState({
			videoVisible: true
		}, function(){
			this.player = videojs(this.videoNode, this.props.video, function onPlayerReady() { });
		})
	}
	componentDidMount() {
		var controller = new ScrollMagic.Controller({loglevel: 0, addIndicators: false})
		if(this.props.parralaxLayers){
			var paralaxAnimPart = new TimelineMax();
			 _.map(this.props.parralaxLayers, function(value, key){
			 	if(value.direction == "from"){
					paralaxAnimPart.from(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}else{
			 		paralaxAnimPart.to(`#paralax_layer_${key}`, 1, value.animation, 0);
			 	}
			})
			var bg_scene = new ScrollMagic.Scene({
				triggerElement: '#bannerHolder', 
				triggerHook: 0,
				duration: 1000,
			})
		    	.addTo(controller)
		    	.setTween(paralaxAnimPart) 
		}	
		var bannerContentTween = new TimelineMax();
		    bannerContentTween.to('#bannerContent', 1, {opacity: 0, ease: Power2.easeOut}, 0)
 
		var bannerContent_scene = new ScrollMagic.Scene({
			triggerElement: '#bannerContent', 
			triggerHook: 0,
			duration: 1000,
		})
	    	.addTo(controller)
	    	.setTween(bannerContentTween)     	

	    	
	}

	componentWillUnmount() {
		if (this.player) {
	  		this.player.dispose()
		}
	}
	render() {
		const styles = {
			hero: {
				backgroundImage: `url(${this.props.heroImg})`,
				backgroundSize: "cover",
			   	backgroundPosition: "50% 50%",
			},
			bannerContent: {
				position: "relative",
			},
			topContainer: {
				textAlign: 'center',
			},
			banner: {
				height: "auto",
				overflow: "hidden",
				padding: "70px 0 0",
				minHeight: "calc(100vh + 50px)",
				overflow: "hidden",
			},
			bannerTitle: {
				marginTop: 50,
				zIndex: 999,
				color: "#fff",
				font: 'normal 1.8rem/.9 "Teko", Arial, sans-serif',
			    textTransform: 'uppercase',
		        fontWeight: 400,
		        letterSpacing: '0.4rem',
			},
			bannerSubTitle: {
				marginTop: 0,
				color: "#fff",
				font: 'normal 6rem/.9 "Rajdhani", Arial, sans-serif',
			    textTransform: 'uppercase',
		        fontWeight: 700,
			},
			bannerSubText: {
				color: "#fff",
			},
			bannerDirections: {
				fontStyle: "italic",
				fontWeight: "bold",
				color: "#fff"
			},
			videoPoster: {
				border: "1px solid #fff",
				maxWidth: "100%",
			},
			paralaxLayer: {
				width: "100%",
				minHeight: "1274px",
				backgroundSize: "cover",
				position: "absolute",
				overflow: "hidden",
				top: 0,
				left: 0,
				zIndex: -1,
			},
			modStats: {
				paddingTop: 80
			},
			absoluteContainer: {
				position: "absolute",
				top: 0,
				left: 0,
				width: "100%",
				height: "1274px",
				overflow: "hidden",
			}
		}
		const {videoVisible} = this.state
		const sectionName = this.props.title.replace(/\W+/g, '-').toLowerCase()

		const paralaxLayers = this.props.parralaxLayers ? _.map(this.props.parralaxLayers, function(value, key){
				return <div id={"paralax_layer_"+key} key={ "paralax_layer_"+ key } style={{ ...styles.paralaxLayer, backgroundImage: `url(${value.src})`}}></div>
			}) : null

		return (
			<div style={ this.props.style } id="bannerHolder">
				<div id={ sectionName } style={ this.props.heroImg ? { ...styles.banner, ...styles.hero} : styles.banner } key="banner">
					<div style={styles.absoluteContainer}>
						{ paralaxLayers }
					</div>
					<div class="banner-container" id="bannerContent" style={ styles.bannerContent }>
						<div class="mdl-grid">
							<div style={ styles.topContainer } class="mdl-cell mdl-cell--12-col">
								{this.props.image && <img src={this.props.image} /> }
								<h1 style={styles.bannerTitle} id="banner_title">{this.props.title} </h1>
								{this.props.subTitle && <h4 style={styles.bannerSubTitle}>{this.props.subTitle}</h4> }
							</div>
							{this.props.subTitle || this.props.text || this.props.directions ?
								<div class={ this.props.video ? "mdl-cell mdl-cell--4-col" : "mdl-cell mdl-cell--12-col" }  >
									{this.props.text && <p style={styles.bannerSubText}>{this.props.text}</p> }
									{this.props.directions && <p style={styles.bannerDirections}>{this.props.directions}</p> }
								</div>
							: null }
							{ this.props.video ?
								<div class="mdl-cell mdl-cell--8-col">
									{ videoVisible ? 
										<div data-vjs-player>
											<video ref={ node => this.videoNode = node } className="video-js vjs-fluid"></video>
										</div>
									: 
										<img style={styles.videoPoster} onClick={this.loadVideo} src={this.props.video.poster} /> 
									}
								</div>
							: null }
						</div>
						{this.props.stats ? 
							<div style={styles.modStats}>
								<ModuleStats stats={this.props.stats} />
							</div>
						: null }
					</div>
				</div>
			</div>
		)	
	}
}
