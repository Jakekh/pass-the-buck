var React = require('react')

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import ListItemAvatar from '@material-ui/core/ListItemAvatar'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon';
import Divider from '@material-ui/core/Divider';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import Toolbar from '@material-ui/core/Toolbar';
import ReactHtmlParser from "react-html-parser"

const styles = {
  root: {
    width: '100%',
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class Glossary extends React.Component {
	constructor(){
		super()
		this.state = { open: false }
	}
	componentWillMount() {
		CourseStore.on( "show_glossary", () => {this.setState({open: true })})
		CourseStore.on( "show_infolink", () => {
			this.setState({open: true })
			this._showTerm(CourseStore.infolinkTerm)
		})
	}
	_closeResources = () => {
		this.setState({
			open: false
		})
	}
	_showTerm = (item) => {
		this.setState({
			activeItem: item,
			open: true
		})
	}
	render() {
		let that = this
		const { open, activeItem } = this.state
		let glossaryEls = _.map(CourseStore.glossaryData, function(item, index){
			return [
				<ListItem button onClick={ that._showTerm.bind(this, item) }>
					<ListItemText class="truncate" primary={ item.term } />
				</ListItem>,
				<Divider />
			]
		})
		const styles = {
			fixedToolbar: {
				position: "absolute",
				top:0,
			    zIndex: 100,
			    width: "100%",
			    backgroundColor: "#fff",
				boxSizing: "border-box",
				borderBottom: "1px rgba(0, 0, 0, 0.3) solid",
			},
			list: {
				marginTop: 64,
				overflowY: "auto",
			},
			centerContent: {
				width: "100%",
				textAlign: "center",
				paddingTop: "20px",
			},
			drawer: {
				height: "auto",
				minHeight: "100vh",
				background: "#fff",
				width: "100%",
				maxWidth: "400px",
			},
			toolbar: {
				background: "#fff",
			    boxShadow: "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px",
			    color: "#ccc",
			    height: "48px",
			},
			toolbarTitle: {
				lineHeight: "48px",
				paddingLeft: "16px",
			},
			active: {
				display: "flex",
			},
			defCont: {
				padding: '12px 24px',
				fontSize: '1.3rem',
			},
			activeTerm: {
				paddingLeft: 0,
				textAlign: 'center',
			},
			backArrow: {
				marginRight: 0,
			}
		}

		return (
			<Drawer class='resources-glossary' open={ open }>
				<Toolbar style={ styles.fixedToolbar }>
					<Typography variant="title" color="inherit" style={{ flex: 1 }}>Glossary</Typography>
		         	<IconButton onClick={this._closeResources}>
		                <Icon className="material-icons">close</Icon>
	              	</IconButton>
			    </Toolbar>
			    { activeItem ?
			    	<div style={ styles.active }>
			    		<div class='glossary-active-term'>
					    	<List style={ styles.list }>
				    			<ListItem button onClick={ () => {this.setState({activeItem: null})} }>
					    			<ListItemIcon style={ styles.backArrow }>
					    				<Icon class="material-icons">arrow_back</Icon>
					                </ListItemIcon>
									<ListItemText style={ styles.activeTerm } primary={ ReactHtmlParser(activeItem.term) } />
								</ListItem>
								<Divider />
							</List>
							<div style={ styles.defCont }>
					    		<div style={ styles.activeDef }>{ ReactHtmlParser(activeItem.def) }</div>
	   						</div>
   						</div>
			    	</div>
			    :
          			<List style={ styles.list }>{ glossaryEls }</List>
          		}
	        </Drawer>
		)
	}
}

Glossary.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Glossary);
