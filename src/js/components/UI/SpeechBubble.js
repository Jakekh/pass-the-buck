var React = require('react')
import ReactHtmlParser from "react-html-parser"

export default class SpeechBubble extends React.Component {
	constructor (props) {
		super(props)
		this.state = {
		}
	}
	 componentWillUnmount() {
		
	}
	componentDidMount () {
		
	}

	render() {
		const { text, direction, icon } = this.props
		//console.log(this.props)
		return (
				<div class={`speech-bubble speech-bubble-${direction}`}><div class='caret'></div><div class='speechIcon'><i class='material-icons'>{ icon }</i></div>{ ReactHtmlParser(text) }</div>
		)	
	}
}
