var React = require('react')
var $ = require('jquery')
import { EventEmitter } from "events"

import Icon from '@material-ui/core/Icon'

import ModalStore from '../../stores/ModalStore'
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

import IconButton from '@material-ui/core/IconButton'
export default class Modal extends React.Component {

	constructor(){
		super()
		this.state = {
			open: false,
			content: []
		}
	}

	componentWillMount = () => {
		ModalStore.on('modal-opened', () => {this.setState({open: true, content: ModalStore.content, onCloseEvent: ModalStore.onCloseEvent})})
	}

	componentWillUnmount = () => {
		ModalStore.removeAllListeners()
	}

	handleClose = () => {
	    this.setState({open: false})
	    this.state.onCloseEvent && ModalStore.fireEvent(this.state.onCloseEvent)
	    $('body').css('overflow', 'overlay')
	}

	render() {
		const { open, content } = this.state

		let modal
		const styles = {
			modal: {
			    background: '#fff',
			    bottom: 0,
			    left: 0,
			    position: 'fixed',
			    right: 0,
			    top: 0,
			    zIndex: 9999,
			    margin: 0,
			    padding: 0,
			    overflowX: "hidden",
			},
			content: {
				bottom: 0,
			    left: 0,
			    position: 'absolute',
			    right: 0,
			    top: 0,
			    overflow: 'auto',
			},
			close: {
			    top: 8,
			    position: 'absolute',
			    right: 24,
			    zIndex: 100,
			},
		}
		if(open){
			$('body').css('overflow', 'hidden')
			modal = 	<div style={ styles.modal } >
							<div class="fullScreenModal" style={ styles.content }>{ content }</div>
							<IconButton onClick={ this.handleClose } style={ styles.close }>
								<Icon>close</Icon>
							</IconButton>
						</div>
		}else{
			modal = null
		}

		return (
				<CSSTransitionGroup
		          transitionName="modal"
		          transitionEnterTimeout={500}
		          transitionLeaveTimeout={300}>{ modal }
		          </CSSTransitionGroup>
		)
	}
}