var React = require('react')
var $ = require('jquery-easing')
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Icon from '@material-ui/core/Icon'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Collapse from '@material-ui/core/Collapse';
import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import LmsStore from '../../stores/LmsStore'
import { withTheme } from '@material-ui/core/styles'
const update = require('immutability-helper')

const styles = {

}


class Sidebar extends React.Component {
	constructor(){
		super()
		this.state = {
			view: CourseStore.returnCourseData(),
			location: LmsStore.returnLocation(),
			moduleProgress: [],
			windowWidth: $(window).width(),
			openModules: _.map(CourseStore.returnCourseData().modules, function(mod, index){
				return {item: index, open: false}
			})
		}
	}
	
	loadModule = (modKey) => {
		const { activeSection } = this.state
		if(this.state.location[0] != modKey){
			CourseActions.loadModule(modKey)
			$(window).scrollTop(0)
		}else{
			if(activeSection != null && activeSection != ""){
				$("html, body").animate({ scrollTop: 0}, 500, "swing")
			}
		}
	}
	resize = () => {
		const { windowWidth } = this.state
		if(windowWidth > 600){
			
			if($(window).width() < 600){
				////console.log("Re-render")
				this.setState({
					windowWidth : $(window).width()
				})
			}
		}else{
			if($(window).width() > 600){
				////console.log("Re-render")
				this.setState({
					windowWidth : $(window).width()
				})
			}
		}
	}
	componentDidMount() {
		CourseStore.on("view_changed", this._updateLocation)
		LmsStore.on("module_completed", this._updateProgress)
		LmsStore.on("lms_data_loaded", this._updateProgress)
		LmsStore.on("user_progress_updated", this._updateProgress)
		window.addEventListener('resize', this.resize)
	}
	
	linkScroll =() => {
		let that = this
		$(window).off("scroll")
		setTimeout(function(){
				let topMenu = $("#sidebarSections"),
			    topMenuHeight = topMenu.outerHeight()+15,
			    menuItems = topMenu.find("li"),
			    scrollItems = menuItems.map(function(){
			      const label = $(this).attr("data-scroll-label").toString()
			      const item = $(`#${label}`)
			      if(item.length) { return item }
			    })
			// Bind to scroll
			$(window).on("scroll", function(){
			   // Get container scroll position
			   let fromTop = $(this).scrollTop()

			   // Get id of current scroll item
			   let cur = scrollItems.map(function(){
			     if ($(this).offset().top < fromTop + 300)
			       return this
			   })
			   // Get the id of the current element
			   cur = cur[cur.length-1]
			   let id = cur && cur.length ? cur[0].id : ""
			   // Set/remove active class
			   //////console.log(id)
			   if(that.state.activeSection != id){
			   	that.setState({
			   		activeSection : id
			   	})
			   }
			})
		}, 200)
	}

	_updateLocation = () => {
		this.setState({
			location: LmsStore.returnLocation(),
			activeSection: null,
		}, function(){
			this.linkScroll()
		})
	}

	_updateProgress = () => {
		//console.log("NEW PROGRESS: ", LmsStore.getModuleProgress() )
		this.setState({moduleProgress: LmsStore.getModuleProgress()})
	}

	goToSection = (sectionTitle, mod) => {
		const { location } = this.state
		let secId = sectionTitle.replace(/\W+/g, '-').toLowerCase()
		console.log( mod, location[0] )
		if(location[0] == mod){
			$("html, body").off().stop().animate({ scrollTop: $('#'+secId).offset().top - 56}, 1000, "swing")
		}else{
			this.loadModule(mod)
			setTimeout(function(){ $("html, body").off().stop().animate({ scrollTop: $('#'+secId).offset().top - 56}, 1000, "swing") }, 200)
		}
	}
	getProgressIcon = (data) => {
		const styles = {
			sec_complete: {
				color: "green",
			},
			sec_incomplete: {
				color: "#ccc",
				zIndex: "999",
			},
			mod_complete: {
				color: this.props.theme.palette.primary.main,
			},
			mod_incomplete: {
				color: "#ccc",
				zIndex: "999",
			},
		}

		let sectionIcons = {
			unViewed: <Icon className="material-icons" style={styles.sec_incomplete}>radio_button_unchecked</Icon>,
			viewed: <Icon className="material-icons" style={styles.sec_incomplete}>radio_button_checked</Icon>,
			complete: <Icon className="material-icons" style={styles.sec_complete}>check_circle</Icon>,
			incomplete: <Icon className="material-icons" style={styles.sec_incomplete}>check_circle</Icon>
		}
		let modIcons = {
			unViewed: <Icon className="material-icons" style={styles.mod_incomplete}>radio_button_unchecked</Icon>,
			viewed: <Icon className="material-icons" style={styles.mod_incomplete}>radio_button_checked</Icon>,
			complete: <Icon className="material-icons" style={styles.mod_complete}>check_circle</Icon>,
			incomplete: <Icon className="material-icons" style={styles.mod_incomplete}>check_circle</Icon>
		}
		let courseProgress = LmsStore.getProgress()
		//console.log("CourseProg: ", courseProgress)
		let complete = false
		if(typeof data == "number"){
			if(courseProgress[data] && courseProgress[data].indexOf(0) == -1){
				complete = true
			}
		}else{
		  complete = courseProgress.length > 1 ? courseProgress[data[0]][data[1]] : false
		}
		
		let icon
		////console.log(complete, typeof data == "number" )

		complete ?
			typeof data == "number" ?
				icon = modIcons.complete
			:
				icon = sectionIcons.complete
		:
			typeof data == "number" ?
				icon = modIcons.incomplete
			:
				icon = sectionIcons.incomplete

		////console.log(icon)
		return icon
	}
	expandMod = (key) => {
		let open = this.state.openModules[key].open
		let openModulesTemp = update( this.state.openModules, { [key]: { $set: {open: !open} }})
		this.setState({
			openModules: openModulesTemp
		})
	}
	buildMenu = (data) => {
		let menuStyles={
			title: {
				
			}
		}
		let that = this
		let rootEls = _.map(data, function(module, key){
			let progIcon = that.getProgressIcon(key)
			return 	<div key={`list_${key}`}>
						<ListItem divider={true} >
				          <ListItemIcon>
				            {progIcon}
				          </ListItemIcon>
				          <ListItemText classes={menuStyles.title} primary={module.title} />
				          { that.state.openModules[key].open ? <IconButton onClick={that.expandMod.bind(this, key)}><Icon>expand_less</Icon></IconButton> : <IconButton onClick={that.expandMod.bind(this, key)}><Icon>expand_more</Icon></IconButton>}
				        </ListItem>
				        { that.buildSections(key, module.sections) }
			        </div>


		})
		return rootEls
	}
	buildSections = (mod, sections) => {
		let menuStyles={
			icon: {
				margin: "0px",
				color: "#f00"
			},
			title: {
				fontSize: "18px",
			}
		}
		const { location, openModules } = this.state
		let that = this
		let sectionEls = _.map(sections, function(section, key){
			////console.log("LOC: ", location[0], key)
			let sectionEl
			let progIcon = that.getProgressIcon([mod, key])
			section.Section ? 
				sectionEl = 
					<Collapse in={ !openModules[mod].open ? false : true } timeout="auto" unmountOnExit key={`col_${key}`}>
						<List component="div" disablePadding>
							<ListItem button divider={true} onClick={ that.goToSection.bind(that, section[Object.keys(section)[0]].title, mod) } >
							<ListItemIcon>
								{progIcon}
							</ListItemIcon>
							<ListItemText primary={section[Object.keys(section)[0]].title} />
								{ that.buildSections(key, module.sections) }
							</ListItem>
						</List>
					</Collapse>
			: null
			return sectionEl
		})
		return sectionEls
	}
	render() {
		let that = this
		const { view, location, activeSection } = this.state
		const { classes, menuOpen } = this.props
		const primaryColor = "#0099CC"
		const displayWidth = $(window).width()
		//console.log(this.props)

		const styles = {
			list: {
				marginTop: 64,
				overflowY: "auto",
				width: 300,
			},
			drawer: {
				/*height: `calc(100vh - 48px`,
				top: "48px",*/
				background: "#fff",
			},
			toolbar: {
				background: "#fff",
				height: "48px",
				position: "absolute",
				top:0,
			    zIndex: 100,
			    width: "100%",
			    backgroundColor: "#fff",
				boxSizing: "border-box",
				borderBottom: "1px rgba(0, 0, 0, 0.3) solid",
			},
			courseTitle: {
				lineHeight: "48px",
				paddingLeft: "20px",
			},
			root: {
			    width: '100%',
			  },
			  flex: {
			    flex: 1,
			  },
			  menuButton: {
			    marginLeft: -12,
			    marginRight: 20,
			  },
		}
		
		
		let listItems = this.buildMenu(view.modules)

		return (
			location ?
				<Drawer variant="persistent" width={ 300 } open={ menuOpen }>
					<Toolbar style={ styles.toolbar }>
						<Typography variant="title" color="inherit" style={ styles.flex }>
				            Menu
			          	</Typography>
						<IconButton onClick={ this.props.toggleMenu } >
					    	<Icon>close</Icon>
					    </IconButton>
				    </Toolbar>
				    <Divider />
				    <div style={ styles.list }>
						<List key={ "sidebar_0" }>
							{ listItems }
						</List>
					</div>
				</Drawer>
			:
			<div style={ styles.sidebarContainer }>
				<p>Loading...</p>
			</div>	
		)	
	}
}

/*Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
}*/

export default withTheme(styles)(Sidebar)