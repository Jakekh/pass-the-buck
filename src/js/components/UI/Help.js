var React = require('react')
var $ = require('jquery-easing')

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ReactHtmlParser from "react-html-parser"

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from '@material-ui/core/Card';
import Icon from '@material-ui/core/Icon'

const styles = theme => ({
  card: {
    maxWidth: 500,
  },
  media: {
    height: 194,
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: "#f00",
  },
});

class Help extends React.Component {
	constructor(){
		super()
		this.state = {
			helpData: require('../../../data/help.json'),
			open: false
		}
	}

	render() {
		const styles = {
			text: {
				paddingTop: '0px',
			},
			card: {
			    minHeight: 145,
			},
			largeCard: {
			    minHeight: 200,
			},
		}
		const { classes } = this.props;
		const content = _.map(this.state.helpData, (value, key) => {
			return (
				<div class={`mdl-cell mdl-cell--${value.cols}-col`}>
					<Card className={classes.card}>
						<CardHeader
	            avatar={
	              value.icon ? <Icon style={ {marginTop: 8} } class="material-icons">{ value.icon }</Icon> : null
	            }
	            title={ <Typography variant="headline" >{ value.title }</Typography> }
	          />
		        <CardContent>
		          <Typography className={classes.title}>{ ReactHtmlParser(value.text) }</Typography>
		        </CardContent>
		      </Card>
				</div>
			)
		})
		return <div class="mdl-grid" key="help">{ content }</div>	
	}
}

Help.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Help);
