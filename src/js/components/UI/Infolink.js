var React = require('react')

import ReactHtmlParser from "react-html-parser"

import Chip from '@material-ui/core/Chip'

import * as CourseActions from '../../actions/CourseActions'

export default class Infolink extends React.Component {
    constructor(){
        super()
    }
    render() {
        const term = this.props.children
        const styles = {
            infolink: {
                display: 'inline',
                color: 'blue',
                textDecoration: 'underline',
                cursor: 'pointer',
                fontSize: 'inherit',
            },
        }

        return (
            <span style={ styles.infolink } onClick={ CourseActions.infolink.bind(this, term) }>{ term }</span>
        )           
    }
}
