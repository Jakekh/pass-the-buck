var React = require('react')
var $ = require('jquery-easing')

import Icon from '@material-ui/core/Icon'

import * as CourseActions from '../../actions/CourseActions'
import CourseStore from '../../stores/CourseStore'
import LmsStore from '../../stores/LmsStore'

var VisibilitySensor = require('react-visibility-sensor')

export default class Footer extends React.Component {
	constructor(){
		super()
		this.state = {
			sensorTriggered: false
		}
	}
	componentDidMount() {
		const location = LmsStore.returnLocation()
		const moduleComplete = LmsStore.getModuleProgress()[location[0]]
		if(!moduleComplete){
			const currentPos = $(window).scrollTop()
			/*$("html, body").stop().animate({ scrollTop: currentPos + 265  }, 600, 'easeInOutQuad', () => {
				setTimeout(() => {
					$(window).on("scroll", function(){ $('#footer-icon').fadeOut() })
				}, 500)
			})*/
			LmsStore.setModuleCopmlete(location[0])
		}else{
			//$('#footer-icon').fadeOut()
		}
	}

	back = () => {
		const location = LmsStore.returnLocation()
		const courseData = CourseStore.returnCourseData()
		courseData.modules[location[0]-1] ? CourseActions.loadModule(location[0]-1) : $("html, body").animate({ scrollTop: 0}, 500, "swing")
	}

	next = () => {
		const location = LmsStore.returnLocation()
		const courseData = CourseStore.returnCourseData()
		courseData.modules[location[0]+1] ? CourseActions.loadModule(location[0]+1) : $("html, body").animate({ scrollTop: 0}, 500, "swing")
	}
	triggerSensor = (isVisible) => {
	 	let progressID = this.props.sectionId.split("_")
	 	progressID[0] = parseInt(progressID[0])
	 	progressID[1] = parseInt(progressID[1])
	 	progressID[2] = parseInt(progressID[2])
		if(isVisible && !this.state.sensorTriggered){
		 	CourseActions.updateProgress(progressID)
		 	this.setState({ sensorTriggered: true })
		}
	}
	render() {
		const location = LmsStore.returnLocation()
		console.log(location)
		const courseData = CourseStore.returnCourseData()
		const lastSection = courseData.modules[location[0]-1] ? true : false
		const nextSection = courseData.modules[location[0]+1] ? true : false
		const lastSectionObj = courseData.modules[location[0]-1] ? courseData.modules[location[0]-1].sections[0].Banner : courseData.modules[location[0]].sections[0].Banner
		const nextSectionObj = courseData.modules[location[0]+1] ? courseData.modules[location[0]+1].sections[0].Banner : courseData.modules[location[0]].sections[0].Banner

		const styles = {
			row: {
				boxShadow: "inset 0 50px 100px -50px rgba(0, 0, 0, 1)",
				/*marginBottom: 440,
		    	height: 80,*/
			},
			fixedContainer: {
				background: "rgba(0,0,0,.6)",
				width: "100%",/*
				position: "",
				bottom: 0,
				zIndex: -1,*/
			},
			container: {
				maxWidth: 1200,
				margin: "0 auto",
				paddingBottom: 50,
			},
			footerTitle: {
				marginTop: 50,
				color: "#fff",
			},
			footerDirections: {
				fontStyle: "italic",
				fontWeight: "bold",
				color: "#fff"
			},
			backNextBox: {
				width: "auto",
				minHeight: 230,
				background: "50% 50% rgba(0,0,0,.1)",
				backgroundSize: "cover",
				height: "auto",
				overflow: "hidden",
				padding: 15,
				zIndex: 100,
				cursor: "pointer",
			},
			backBox: {
				color: "#fff",
				backgroundImage: `url(${lastSectionObj.heroImg})`,
			},
			nextBox: {
				color: "#fff",
				textAlign: 'right',
				backgroundImage: `url(${nextSectionObj.heroImg})`,
			},
			gridBox:{
			    overflow: "hidden",
			},
			h4: {
				margin: 0
			},
			h3: {
				margin: "12px 0px"
			},
			footerIconHold:{
				position: "absolute",
				width: "100%",
				height: "50px",
				margin: "auto",
			},
			iconCont: {
				textAlign: "center",
				paddingTop: 15,
			},
			arrowIcon: {
			    height: 44,
			    width: 44,
			    color: "#fff",
			},
		}

		this.backBtn = <div onClick={this.back} style={{...styles.backNextBox, ...styles.backBox}}><h4 style={styles.h4}>{lastSection ? 'Previous Lesson' : 'Restart Lesson'}</h4><h3 style={styles.h3}>{lastSectionObj.title}</h3></div>
		this.nextBtn = <div onClick={this.next} style={{...styles.backNextBox, ...styles.nextBox}}><h4 style={styles.h4}>{nextSection ? 'Next Lesson' : 'Restart Lesson'}</h4><h3 style={styles.h3}>{nextSectionObj.title}</h3></div>
		const sectionName = this.props.title.replace(/\W+/g, '-').toLowerCase()

		return (
			<VisibilitySensor partialVisibility={'top'} delayedCall={true} onChange={this.triggerSensor} scrollDelay={200} scrollCheck={true}>
			<div style={styles.row} id={sectionName} key="footer">
				<div style={styles.fixedContainer}>
					<div style={styles.footerIconHold}>
						<div id="footer-icon" style={styles.iconCont}>
							<Icon style={styles.arrowIcon} >expand_more</Icon>
						</div>
					</div>
					<div style={styles.container}>
						<div class="mdl-grid">
							<div class="mdl-cell mdl-cell--12-col">
								<h1 style={styles.footerTitle}>{`You have completed Lesson ${location[0]+1}`}</h1>
								<p style={styles.footerDirections}>{this.props.directions}</p>
							</div>
							<div style={styles.gridBox} class="mdl-cell mdl-cell--6-col">
								{this.backBtn}
							</div>
							<div style={styles.gridBox} class="mdl-cell mdl-cell--6-col">
								{this.nextBtn}
							</div>
						</div>
					</div>
				</div>
			</div>
			</VisibilitySensor>
		)
	}
}
