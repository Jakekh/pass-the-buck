var React = require('react')
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '@material-ui/core/IconButton'
import Icon from '@material-ui/core/Icon'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import Button from '@material-ui/core/Button'
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Help from './Help'

import LmsStore from '../../stores/LmsStore'
import MsgStore from '../../stores/MsgStore'
import CourseStore from '../../stores/CourseStore'
import * as CourseActions from '../../actions/CourseActions'

const styles = theme => ({
	title: {
		flex: 1,
	},
  menuItem: {
    /*'&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },*/
  },
  primary: {},
  icon: {},
});


class Toolbar_ui extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			mode: CourseStore.mode,
			menuOpen: props.menuOpen,
			reviewSummary: null,
		}
	}
	exit = () => {
		MsgStore.createMsg({title: "Exit", message: "Are you sure you want to exit the course?", type: 'prompt', submit: LmsStore.saveSuspendData.bind(this, 'exit')})
		this.setState({anchorEl: null})
		this.handleClose()
	}

	help = () => {
		MsgStore.createMsg({title: "Help", message: <Help />, type: 'help', size: 'wider', customActions: [{label: "Close", action: null}]})
		this.setState({anchorEl: null})
		this.handleClose()
	}
	_resources = () => {
		CourseStore.showResources()
		this.handleClose()
	}
	_glossary = () => {
		CourseStore.showGlossary()
		this.handleClose()
	}
	reviewSummary = () => {
		console.log(CourseStore.assessmentRemediation)
		MsgStore.createMsg({title: "Review Summary", message: CourseStore.assessmentRemediation, type: 'popup-unparsed', size: 'wider', customActions: [{label: "Close", action: null}]})
		this.setState({anchorEl: null})
	}
	componentWillMount(){
		CourseStore.on("assessment_review_change", this.updateToolbar)
		CourseStore.on("mode_changed", this.changeMode)
	}
	componentWillUnmount(){
		CourseStore.removeListener("assessment_review_change", this.updateToolbar)
	}
	changeMode = () => {
		this.setState({
			mode: CourseStore.mode
		})
	}
	handleClick = event => {
  	this.setState({ anchorEl: event.currentTarget });
	}

	handleClose = () => {
  	this.setState({ anchorEl: null });
	}
	updateToolbar = () => {
		this.setState({
			reviewSummary: CourseStore.assessmentRemediation ? CourseStore.assessmentRemediation : null
		})
	}
	componentWillReceiveProps(nextProps){
		console.log(nextProps)
		nextProps && this.state.menuOpen != nextProps.menuOpen ?
			this.setState({menuOpen: nextProps.menuOpen })
		: null
	}
	promptExitTestout = () => {
		MsgStore.createMsg({
			title: "Exit Test Out",
			message: "Are you sure you want to exit the Test out? You will be forced to take the whole course.",
			type: 'prompt',
			submit: CourseActions.exitTestout.bind(this)})
	}
	menuOpenAttempt = () => {
		if(this.state.mode == "testout"){
			MsgStore.createMsg({
				title: "Alert",
				message: "You are currently attempting to test out of the course. You cannot view the menu while in this mode. Please exit testout if you would like to take the course.",
				type: 'prompt',
				customActions: [{
					label: "Close",
					action: null
				}],
			})
		}else{
			this.props.toggleMenu()
		}
	}
	render() {
		const { course, classes } = this.props
		const { menuOpen, reviewSummary, mode } = this.state
		const styles = {
			fixedToolbar: {
				position: "fixed",
				top:0,
			    zIndex: "100",
			    width: "100%",
				transition: 'width 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
			},
			menuOpenToolbar: {
				position: "fixed",
				top:0,
			    zIndex: "100",
				width: "calc(100% - 300px)",
				transition: 'width 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
			},
			toolbar: {
				transition: "width 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms",
				background: "#fff",
				height: "48px",
			    boxShadow: "rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px",
			    color: "#fff",
			    zIndex: "99999"
			},
			courseTitle: {
				lineHeight: "48px",
				paddingLeft: "20px",
				color: "#fff",
			},
			logo: {
				maxWidth: "250px",
				padding: "13px",
				background: "#fff"
			},
			toolbarMenu: {

			},
			toolbarButton: {
				margin: "6px",

			}
		}

		return (
			<div style={ !menuOpen ? styles.fixedToolbar : styles.menuOpenToolbar }>
				<Toolbar style={ styles.toolbar }>
				{ !menuOpen ? <IconButton onClick={ this.menuOpenAttempt }>
			    	<Icon>menu</Icon>
			    </IconButton> : null }
			    <Typography variant="title" className={ classes.title }>{ course.title }</Typography>

					{ reviewSummary ?
						<Button primary={ true } style={ styles.toolbarButton } onClick={ this.reviewSummary.bind(this) }>Review Summary</Button>
						: null
					}
					{ mode == "testout" ?
						<Button primary={ true } style={ styles.toolbarButton } onClick={ this.promptExitTestout.bind(this) }>Exit Testout</Button>
						: null
					}
        	<IconButton onClick={ this.handleClick }>
        		<Icon>layers</Icon>
      		</IconButton>

        	<Menu open={ Boolean(this.state.anchorEl) } anchorEl={ this.state.anchorEl } onClose={ this.handleClose } >
        		<MenuItem className={ classes.menuItem } onClick={ this.help }>
          		<ListItemIcon className={ classes.icon }>
		            <Icon>help_outline</Icon>
			        </ListItemIcon>
			        <ListItemText classes={{ primary: classes.primary }} inset primary="Help" />
        		</MenuItem>
            <MenuItem className={ classes.menuItem } onClick={ this.exit }>
            	<ListItemIcon className={ classes.icon }>
		            <Icon>exit_to_app</Icon>
			        </ListItemIcon>
			        <ListItemText classes={{ primary: classes.primary }} inset primary="Exit" />
		        </MenuItem>
	            {/*<MenuItem className={ classes.menuItem } onClick={ this._resources }>
	            	<ListItemIcon className={ classes.icon }>
			            <Icon>insert_link</Icon>
			        </ListItemIcon>
			        <ListItemText classes={{ primary: classes.primary }} inset primary="Resources" />
		        </MenuItem>*/}
            <MenuItem className={ classes.menuItem } onClick={ this._glossary }>
            	<ListItemIcon className={ classes.icon }>
		           	<Icon>sort_by_alpha</Icon>
			        </ListItemIcon>
			        <ListItemText classes={{ primary: classes.primary }} inset primary="Glossary" />
		        </MenuItem>
        	</Menu>
		    </Toolbar>
	    </div>
		)
	}
}

Toolbar_ui.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Toolbar_ui);
