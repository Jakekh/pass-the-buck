var React = require('react')
var $ = require('jquery')

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import InteractionBumper from '../UI/InteractionBumper'
import update from 'immutability-helper';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';

import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import CourseStore from '../../stores/CourseStore'
import MsgStore from '../../stores/MsgStore'

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export default class AssessmentReview extends React.Component {

	buildReviewTable = (data) => {
		let that = this
		let styles = {
			cellStyle: {
				fontSize: "18px"
			},
			cell: {
				overflow: "hidden",
		    	whiteSpace: "initial",
		    	textOverflow: "initial",
	    	    wordWrap: "break-word",
	    	    padding: "10px 24px",
			},
			width_20: {
				width: "20%"
			},
			buttoncell:{
				width: "80px"
			}
		}
		let table =
		<Table>
        <TableHead>
          <TableRow>
            <TableCell style={{ ...styles.width_20, ...styles.cellStyle }}>Question ID</TableCell>
            <TableCell style={styles.cellStyle}>Question Text</TableCell>
            <TableCell style={styles.cellStyle}>Your Answer</TableCell>
            <TableCell style={{ ...styles.cell, ...styles.buttoncell, ...styles.cellStyle }}>&nbsp;</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {
	       	_.map(data, function(question, index){
	       		return <TableRow key={`assRow_${index}`} >
			            <TableCell style={{...styles.cell, ...styles.width_20, ...styles.cellStyle}}>{question.id}</TableCell>
			            <TableCell style={{...styles.cell, ...styles.cellStyle}}>{question.description}</TableCell>
			            <TableCell style={{...styles.cell, ...styles.cellStyle}}>{question.learner_response}</TableCell>
			            <TableCell style={{...styles.cell, ...styles.buttoncell, ...styles.cellStyle}}><Button onClick={that.props.reviewQuestion.bind(that, question, index)}>Review</Button></TableCell>
			          </TableRow>
	        })
   		}
        </TableBody>
      </Table>
      return table
	}

	render() {
		const {scormInts} = this.props
		let that = this
		const styles = {
			cellStyle: {
				fontSize: "18px"
			}
		}
		let table = this.buildReviewTable(scormInts)
		return (
			<Card style={{width: "100%"}}>
				<CardHeader
          title="Assessment Review"
          subheader={<div style={styles.cellStyle}>Before submitting your answers, take a minute to review them.</div>}
        />
        <CardContent>
  				<div>{ table }</div>
        </CardContent>
        <CardActions>
          <Button color={"primary"} variant={"raised"} onClick={this.props.completeAssessment.bind(this)}>Submit Assessment</Button>
        </CardActions>
      </Card>
		)
	}
}
