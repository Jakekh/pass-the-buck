var React = require('react')

import ReactHtmlParser from "react-html-parser"

import Icon from '@material-ui/core/Icon'
import InformationalPane from "./InformationalPane"
import ClickandDiscover from "./ClickandDiscover"
import Question from "./Question"
import InteractionBumper from '../UI/InteractionBumper'
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import ModalStore from '../../stores/ModalStore'
import { withTheme } from '@material-ui/core/styles'
import ButtonBase from '@material-ui/core/ButtonBase'

class CaseStudies extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		console.log(savedState)
		if(savedState != undefined){
			this.state = {
				caseStudieEls: null,
				completed: savedState.c,
			}
		}else{
			console.log(props)
			this.state = {
				caseStudieEls: null,
				completed: Array(props.caseStudies.length).fill(0),
			}
		}
	}
	buildColumn = (props, numColumns, key) => {
		const styles = {
			completeIconHolder: {
				width: "100%",
				textAlign: "center",
				padding: "20px 0",
			},
			completeIcon: {
				color: "#fff",
				fontSize: "30px",
			},
			caseStudyItem: {
				padding: "15px",
			}
		}
		let gridClass = 'mdl-cell mdl-cell--12-col'
		if(props.forceCol){ 
			gridClass = 'mdl-cell mdl-cell--'+props.forceCol.desktop+'-col mdl-cell--'+props.forceCol.tablet+'-col-tablet mdl-cell--'+props.forceCol.tablet+'-col-phone'
		}else{
			switch(numColumns){
				case 1:
					gridClass = 'mdl-cell mdl-cell--12-col'
				break
				case 2:
					gridClass = 'mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet'
				break
				case 3:
					gridClass = 'mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet'
				break
				case 4:
					gridClass = 'mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet'
				break
			}
		}
		return <div class={gridClass} key={"col_"+ key} style={styles.caseStudyItem}>
			<ButtonBase className="relative img-labeled" onClick={this.showCaseStudy.bind(this, key)}>
				<div className="img-title" >
					{props.title} 
					{ this.state.completed[key] == 1 ? 
						<div style={styles.completeIconHolder}><Icon style={styles.completeIcon}>check_circle</Icon></div>
				 	: null }
				 </div>
				<img className="img-responsive" src={props.img} />
			</ButtonBase>
		</div>
	}
	showCaseStudy = (caseNum) => {
		let that = this
		const { caseStudies, componentId } = this.props
		console.log(caseNum)
		const components = {
		    InformationalPane: InformationalPane,
		    ClickandDiscover: ClickandDiscover,
		    Question: Question,
		}
		
		let childrenIndex = 0
		const sectionComponents = _.map( caseStudies[caseNum].content, function(value, childKey){
			childrenIndex ++
			let subComps =_.map( value, function(component, compKey){
				console.log(compKey)
				const ComponentType = components[compKey]
				console.log("Container: ", component.addContainer)
				if( component.addContainer ){
					return <div class="container"><ComponentType  muiTheme={that.props.muiTheme} { ...component } 
	    				threshold={that.props.threshold ? that.props.threshold : null} 
	    				key={ `${compKey}_${childrenIndex}_${caseNum}` }
	    				componentId={`${componentId}_${childKey}_${caseNum}`}/></div>
				}else{
	    			return <ComponentType  muiTheme={that.props.muiTheme} { ...component } 
	    				threshold={that.props.threshold ? that.props.threshold : null} 
	    				key={ `${compKey}_${childrenIndex}_${caseNum}` }
	    				componentId={`${componentId}_${childKey}_${caseNum}`}/>
   				}
	    	})
			 return subComps
		})
		let newAry = this.state.completed
		newAry[caseNum] = 1

		newAry.indexOf(0) == -1 ? 
			that.props.getNextThreshold ? that.props.getNextThreshold() : null
		: null
		that.setState({ completed: newAry })

		sectionComponents.push(<InteractionBumper style={ {position: 'absolute', bottom: 0} } text={caseStudies[caseNum].directions} />)

		ModalStore.createModal({ content: sectionComponents })
		CourseActions.saveInteraction({id: that.props.componentId, c: that.state.completed})
	}
	render() {
		const{ title, caseStudies } = this.props
		const {caseStudieEls, completed} = this.state

		let numColumns = caseStudies.length
		let that = this
		let children = _.map( caseStudies, function(value, key){
			return that.buildColumn(value, numColumns, key)
		})

		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			chip: {
				borderRadius: '20px',
			    height: '40px',
			    transition: 'all .5s'
			},
			infoPane: {
				background: this.props.background ? "url("+ this.props.background.url + ")" : "transparent",
				backgroundSize: "cover",
				position: "relative",
				borderBottom: this.props.background ? "50px solid #fff" : "none",
			},
			directions: {
				textAlign: "left",
				maxWidth: '800px',
				margin: "15px auto",
				marginTop: "0px",
			}
		}

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null
		return (
			<div>
				<div class={this.props.shadowed ? "shadowSection" : null}>
					<div style={styles.infoPane} class={gridClass} key={title}>
						{ this.props.icon || this.props.titl || this.props.directions ?
							<div style={styles.centeredContent}>
								{ this.props.icon ? <div class="golfBall"><Icon className="material-icons" >{this.props.icon}</Icon></div> : null }
								{ this.props.title ? <h2>{this.props.title}</h2> : null }
								{ this.props.directions ? <h4>{this.props.directions}</h4> : null }
							</div>
						: null }
						{ children }
					</div>
				</div>
			</div>
		)	
	}
}
export default withTheme()(CaseStudies)