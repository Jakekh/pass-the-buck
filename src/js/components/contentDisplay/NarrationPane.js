

var React = require('react')

import ReactHtmlParser from "react-html-parser"
import JsxParser from 'react-jsx-parser'

import AudioPlayer from "../UI/AudioPlayer"
import Infolink from "../UI/Infolink"

export default class InformationalPane extends React.Component {
	constructor(){
		super()
	}

	render() {
		const { title, text, img, audio, leftLine, whitebg, style, titleStyle, outerStyle } = this.props

		const styles = {
			narrPane: {
				color: "#333",
			    marginBottom: "6rem",
			    position: "relative",
			},
			text: {
				font: '300 1.6rem/2 "Roboto", Arial, sans-serif',
				color: whitebg ? "#0d1017" : "#fff",
			    letterSpacing: '.2rem',
		        margin: '0px 5.75rem',
			    border: whitebg ? 'none' : '1px solid #fff',
			    padding: '2rem',
			},
			imgCont: {
				textAlign: "center"
			},
			img: {
				borderRadius: "50%",
			    position: "relative",
			    maxWidth: 80,
			    marginTop: -70,
			    boxShadow: "rgba(0, 0, 0, 0.2) 0px 1px 5px 0px, rgba(0, 0, 0, 0.14) 0px 2px 2px 0px, rgba(0, 0, 0, 0.12) 0px 3px 1px -2px"
			},
		}

		return (
			<div class="fullWidth" key={ "narrationPane_"+Math.floor((Math.random() * 1000) + 1)  } >
				<div style={{ ...styles.narrPane, ...this.props.outerStyle }} class="container mdl-grid" >
					<div class="mdl-cell mdl-cell--12-col" >
						{ img && <div style={ styles.imgCont }><img style={ styles.img } src={ img } class="img-responsive" /></div> }
						<div class={ leftLine ? 'left-line' : null } style={{ ...styles.text, ...this.props.style }} >
							{ title && <h3 class='section-title' style={ this.props.titleStyle }>{ title }</h3> }
							{ text && <JsxParser components={{ Infolink }} jsx={ text } showWarnings={true} />}
							{ audio && <AudioPlayer whitebg={ whitebg } audio={ audio } /> }
						</div>
					</div>
				</div>
			</div>
		)	
	}
}
