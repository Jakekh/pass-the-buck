var React = require('react')
var $ = require('jquery')
var _ = require('lodash')
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import ReactHtmlParser from "react-html-parser"
import update from 'immutability-helper';
import LmsStore from '../../stores/LmsStore'
import * as CourseActions from '../../actions/CourseActions'
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import InteractionBumper from '../UI/InteractionBumper'
import { withTheme } from 'material-ui/styles';

class Question extends React.Component {
	constructor(props){
		super(props)
		console.log("CONSTRUCTOR: ", props.componentId, LmsStore.returnInteractionData(props.componentId) )
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined && props.settings.knowledgeCheck){
			this.state = {
				cbSelection: savedState.cbSelection,
				cbCorrect: savedState.cbCorrect,
				cbSelection_text: savedState.cbSelection_text,
				localSelection: savedState.selection,
				tries: savedState.tries,
				id: props.componentId,
				userData : savedState.userData,
				selection: savedState.selection,
				review: false,
			}
		}else{
			this.state = {
				id: props.componentId,
				cbSelection: props.selection ? props.selection : [],
				cbCorrect: false,
				cbSelection_text: cbSelection_text,
				localSelection: null,
				tries: props.settings.tries ? props.settings.tries : 99,
				review: false,
			}
		}
		//console.log(savedState)
		let cbSelection_text = []
		if(props.selection){
			cbSelection_text = _.map(props.selection, function(selection, index){
				return props.questionData.distractors[selection].value
			})
		}
		
	}

	//Only Used for isolated knowledge checks where answerQuestion isnt passed through props.
	answerQuestion(correctness, selection, answeredText, question){
		let that = this
		const { questionData } = this.props
		const { userData } = this.state

		let userDataUpdate = update(userData, { $set:  { correctness: correctness, selection: selection } })

		let correctDistractors = _.filter(questionData.distractors, function(d){ return d.correct })
		let latency = new Date().getTime() - this.state.startTime
		let scormObj = {
			weight: 1,
			id: "knowledgeCheck_"+this.props.componentId,
			type: "fill-in",
			objId: 0, //Not dealing with objectives right now.
			timestamp: new Date().toISOString().slice(11, 22),
			correct_responses: _.map(correctDistractors, function(value, key){return value }), 
			learner_response: answeredText,
			result: correctness ? "correct" : "wrong",
			latency: LmsStore.returnFormattedTime(latency),
			description: questionData.questionText
		}

		LmsStore.saveSCORMinteraction(scormObj)
		
		CourseActions.saveInteraction({
			cbSelection: this.state.cbSelection,
			cbCorrect: this.state.cbCorrect,
			cbSelection_text: this.state.cbSelection_text,
			localSelection: this.state.selection,
			tries: this.state.tries,
			id: this.props.componentId,
			userData : userDataUpdate,
			selection: this.state.selection,
		})

		this.setState({
			tries: this.state.tries-1,
			localSelection: selection,
			startTime: new Date().getTime(),
			userData: userDataUpdate,
			localCorrectness: correctness,
			feedbackText: correctness ? this.props.feedback.correct.value : this.props.feedback.incorrect.value,
			feedbackOpen: true,
			review: true,
		}, function(){
			if(this.props.threshold && correctness || this.props.threshold && this.state.tries <= 0){ 
				//this.showCorrect(questionData.distractors)
				console.log("GETTING NEXT THRESHOLD")
				this.props.getNextThreshold() 
			}
			!correctness && this.state.tries <= 0 && this.props.settings.showFeedback ?
				this.showCorrect(questionData.distractors)
			: null
		})
	}
	
	componentWillReceiveProps = (nextProps) => {
		//console.log("Got Props: ", nextProps.selection )
		if(nextProps.selection){
			let cbSelection_text = []
			cbSelection_text = _.map(nextProps.selection, function(selection, index){
				return nextProps.questionData.distractors[selection].value
			})
			this.setState({
				cbSelection: nextProps.selection != null ? nextProps.selection : [],
				cbCorrect: false,
				cbSelection_text: cbSelection_text,
			})
		}else{
			this.setState({
				cbSelection: [],
				cbCorrect: false,
				cbSelection_text: null,
			})
		}
	}
	submitAnswer = ( key ) => {
		const { cbSelection } = this.state
		const { questionData } = this.props

		const existingKey = _.indexOf(cbSelection, key)
		let newSelection

		if( existingKey != -1 ){
			newSelection = update(cbSelection, {$splice: [[existingKey, 1]] })
		} else {
			newSelection = update(cbSelection, {$push: [key]})
		}
		
		let ansAry = []
		 _.map(questionData.distractors, function(item, index){
			if( item.correct ){ ansAry.push(index) } 
		})

		let cbSelection_text = _.map(newSelection, function(selection, index){
			return questionData.distractors[selection].value
		}) 

		let correctness = _.isEmpty(_.xor(ansAry, newSelection))

		this.setState({ cbSelection: newSelection, cbCorrect: correctness, cbSelection_text: cbSelection_text.join(", ") })
	}

	closeFeedback = () => {
	    this.setState({feedbackOpen: false});
  	}

  	getDistractorStyle = (cbSelection, key, type) => {
  		const { questionData, shadowed } = this.props
  		const { tries } = this.state
  		let showFeedback = this.state.review
  		let selected = _.indexOf(cbSelection, key) != -1 
  		let correct = _.indexOf(cbSelection, key) != -1 && questionData.distractors[key].correct
  		const styles = {
			distractor: {
				cursor: tries > 0 ? "pointer" : "initial",
				width: "auto",
				minWidth: "340px",
				padding: "15px 10px",
				display: "inline-block",
				marginTop: "10px",
				border: "2px solid transparent",
			},
			distractor_selected: {
				border: "2px solid transparent",
				borderRadius: "2px",
				background: shadowed ? this.props.theme.palette.primary.light : this.props.theme.palette.primary.dark ,
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_correct: {
				background: "rgba(35, 154, 35, .6)",
				borderRadius: "2px",
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_incorrect: {
				background: "rgba(179, 18, 18, .6)",
				borderRadius: "2px",
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractorIcon: {
				position: "relative",
				top: "5px",
				fontSize: "20px",
				color: shadowed ? "#fff" : "#ddd" ,
				display: "inline-block",
			},
			selectedIcon: {

			},
			selectedIcon_incorrect: {
				color: shadowed ? "#fff" : "#ddd" ,
			},
			selectedIcon_correct: {
				color: shadowed ? "#fff" : "#ddd" ,
			},

		}

		let styleObj
  		if(selected){
	  		if(showFeedback){
  				if(correct){
  					type == "distractor" ? 
  						styleObj = { ...styles.distractor, ...styles.distractor_selected_correct }
  					: 
  						styleObj = { ...styles.distractorIcon, ...styles.selectedIcon_correct }
  				}else{
  					type == "distractor" ? 
  						styleObj = { ...styles.distractor, ...styles.distractor_selected_incorrect }
					:
						styleObj = { ...styles.distractorIcon, ...styles.selectedIcon_correct }
  				}
	  		}else{
	  			type == "distractor" ? 
	  				styleObj = { ...styles.distractor, ...styles.distractor_selected }
  				:
  					styleObj = { ...styles.distractorIcon, ...styles.selectedIcon }
	  		}
	  	}else{
	  		type == "distractor" ? 
	  			styleObj = styles.distractor
  			:
  				styleObj = styles.distractorIcon
	  	}

	  	return styleObj
  	}
 	retry = () => {
 		this.setState({review: false, cbSelection: []});
 	}
 	showCorrect = (distractors) => {
 		let that = this
 		console.log("SHOWING CORRECT: ", distractors)
 		let corSel = []
 		corSel = _.map(distractors, function(d, index){ return d.correct ? index : null })
 		console.log("CORSEL: ", corSel)
 		that.setState({cbSelection: corSel}, function(){
 			console.log("CORRECT STATE: ", this.state.cbSelection)
 		});
 	}
	render() {
		const { questionData, settings, answerQuestion, correctness, selection, feedback, shadowed } = this.props
		const { cbSelection, cbCorrect, cbSelection_text, localSelection, localCorrectness, tries, review } = this.state
		//console.log("Current Selection: ", cbSelection)
		let submitFunction = this.props.answerQuestion ? this.props.answerQuestion : this.answerQuestion
		let that = this
		const styles = {
			fullWidth: {
				width: "100%",
			},
			assessmentHolder: {
				minHeight: "calc( 100vh - 120px )",
			},
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},			
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			questionText: {
				fontSize: "24px",
				marginBottom: "10px",
		    	lineHeight: "30px",
		    	width: "100%",
			},
			distractor: {
				cursor: tries > 0 ? "pointer" : "initial",
				width: "auto",
				minWidth: "340px",
				padding: "15px 10px",
				display: "inline-block",
				marginTop: "10px",
				border: "2px solid transparent",
			},
			distractor_selected: {
				border: "2px solid transparent",
				borderRadius: "2px",
				background: shadowed ? this.props.theme.palette.primary.light : this.props.theme.palette.primary.dark ,
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_correct: {
				background: "rgba(35, 154, 35, .6)",
				borderRadius: "2px",
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_incorrect: {
				background: "rgba(179, 18, 18, .6)",
				borderRadius: "2px",
				paddingRight: 50,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractorIcon: {
				position: "relative",
				top: "5px",
				fontSize: "20px",
				color: shadowed ? "#fff" : "#ddd" ,
				display: "inline-block",
			},
			selectedIcon: {

			},
			selectedIcon_incorrect: {
				color: shadowed ? "#fff" : "#ddd" ,
			},
			selectedIcon_correct: {
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractorText: {
				display: "inline",
				paddingLeft: "10px",
			    marginLeft: "-30px",
			    position: "relative",
			    left: "30px",
			},
			feedback: {
				padding: "20px",
				margin: "40px 0",
				width: "100%",
			},
			questionHold: {
				minHeight: "400px",
				width: "100%",
			},
		}
		let sel = localSelection != null ? localSelection : selection
		let cor = correctness != null ? correctness : localCorrectness
		let feedbackText = feedback
		if(typeof(feedbackText) == "object" && feedback != null){
			sel != null && cor  ?
				feedbackText = feedback.correct.value
			: 	
				sel != null && tries > 0 ? 
					feedbackText = feedback.tryagain && feedback.tryagain.value ? feedback.tryagain.value : null
				: sel != null && tries <= 0 ? 
					feedbackText = feedback.incorrect.value 
				: feedbackText = null
		}
		let distractors =  _.map(questionData.distractors, function(option, key){
			if(questionData.type != "cb"){
				return 	[<div 
							style={ 
								sel == key && cor && settings.showFeedback ? 
									{ ...styles.distractor, ...styles.distractor_selected_correct } 
								: 
									sel == key && !cor && settings.showFeedback ? 
										{ ...styles.distractor, ...styles.distractor_selected_incorrect } 
									:  
										sel == key ? 
											{ ...styles.distractor, ...styles.distractor_selected }
										:
											styles.distractor
									 } 
							key={ "dis_"+ key } 
							onTouchTap={ tries > 0 && !cor || that.props.answerQuestion ? submitFunction.bind(that, option.correct, key, option.value, questionData) : null }>
							<span >
								<Icon className="material-icons" 
									style={ 
										sel == key && cor && settings.showFeedback ? 
											{ ...styles.distractorIcon, ...styles.selectedIcon_correct } 
										: 
											sel == key && !cor && settings.showFeedback ? 
												{ ...styles.distractorIcon, ...styles.selectedIcon_incorrect } 
											:  
												sel == key ? 
													{ ...styles.distractorIcon, ...styles.selectedIcon }
												:
													styles.distractorIcon
											 } >
										{ sel == key ? "radio_button_checked" : "radio_button_unchecked" } 
								</Icon>
							</span>
							<div style={styles.distractorText}>
								{ option.value }
							</div>
						</div>, <br />]
			}else{
				//console.log(cbSelection, key, _.indexOf(cbSelection, key) )
				return 	[<div style={ that.getDistractorStyle(cbSelection, key, "distractor")} 
							key={ "dis_"+ key } 
							onTouchTap={ tries > 0 && !review || that.props.answerQuestion ? that.submitAnswer.bind(that, key) : null  }>
							<span >
								<Icon className="material-icons" 
									style={ that.getDistractorStyle(cbSelection, key, "icon")} >
										{ _.indexOf(cbSelection, key) != -1 ? "check_box" : "check_box_outline_blank"  } 
								</Icon>
							</span>
							<div style={styles.distractorText}>
								{ option.value }
							</div>
						</div>, <br />]
			}
		})
		
		let rootClass = settings.knowledgeCheck ? "container" : null
		return (
			<div>
				<div class={rootClass} style={styles.questionHold}>
					
					<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--12-col">
							<div style={styles.questionText}>{ questionData.questionText } </div>
						</div>
						<div class="mdl-cell mdl-cell--6-col">
							{ distractors }
						</div>
						<div class="mdl-cell mdl-cell--6-col">
							{ questionData.assoc_img ? <img style={{maxWidth: "100%", width: "100%"}} src={questionData.assoc_img} /> : null }
						</div>
						{ feedback && feedbackText && settings.showFeedback ?
							<Dialog style={{zIndex: "9999999"}} onClose={this.handleClose} aria-labelledby="simple-dialog-title" open={this.state.feedbackOpen}>
				        <DialogTitle id="simple-dialog-title">Feedback</DialogTitle>
				        	<DialogContent>
				            <DialogContentText id="alert-dialog-description">
				              { feedbackText }
				            </DialogContentText>
				          </DialogContent>
				          <DialogActions>
				            <Button onClick={this.closeFeedback} color="primary">
				              Close
				            </Button>
				          </DialogActions>
					      </Dialog> : null }
					</div>
					{questionData.type == "cb" && !review ? 
						<div><Button color={shadowed ? "default" : "primary"} variant={"raised"} onTouchTap={ submitFunction.bind(this, cbCorrect, cbSelection, cbSelection_text, questionData)}>Submit Answer</Button></div>
					: 
						questionData.type == "cb" && review && tries > 0 ?
							<div><Button color={shadowed ? "default" : "primary"} variant={"raised"} onTouchTap={ this.retry.bind(this) }>Retry</Button></div>
						: null }
				</div>
				{settings.knowledgeCheck && tries > 0 && !cor && this.props.settings.thresholdDirections ? 
					<InteractionBumper style={{background: "#c74c4c", color: "#fff"}} text={this.props.settings.thresholdDirections} />
					: null}
			</div>	
		)	
	}
}

export default withTheme()(Question);