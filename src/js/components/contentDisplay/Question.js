var React = require('react')
var $ = require('jquery')
var _ = require('lodash')
import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import ReactHtmlParser from "react-html-parser"
import update from 'immutability-helper';
import LmsStore from '../../stores/LmsStore'
import * as CourseActions from '../../actions/CourseActions'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import InteractionBumper from '../UI/InteractionBumper'
import { withTheme } from '@material-ui/core/styles';

class Question extends React.Component {
	constructor(props){
		super(props)
		//console.log("CONSTRUCTOR: ", props.componentId, LmsStore.returnInteractionData(props.componentId) )
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined && props.settings.knowledgeCheck){
			this.state = {
				cbSelection: savedState.cbSelection,
				cbCorrect: savedState.cbCorrect,
				cbSelection_text: savedState.cbSelection_text,
				localSelection: savedState.selection,
				tries: savedState.tries,
				id: props.componentId,
				userData : savedState.userData,
				selection: savedState.selection,
				showIndicator: false,
				startTime: new Date().getTime(),
			}
		}else{
			this.state = {
				id: props.componentId,
				cbSelection: props.selection ? props.selection : [],
				cbCorrect: false,
				cbSelection_text: cbSelection_text,
				localSelection: null,
				tries: props.questionData.distractors.length == 2 ? 1 : props.settings.tries ? props.settings.tries : 99,
				showIndicator: false,
				startTime: new Date().getTime(),
			}
		}
		//console.log(savedState)
		let cbSelection_text = []
		if(props.selection){
			cbSelection_text = _.map(props.selection, function(selection, index){
				return props.questionData.distractors[selection].value
			})
		}

	}

	//Only Used for isolated knowledge checks where answerQuestion isnt passed through props.
	answerQuestion(correctness, selection, answeredText, question){
		console.log("yo")
		let that = this
		const { questionData, isLastQuestion } = this.props
		const { userData } = this.state

		let userDataUpdate = update(userData, { $set:  { correctness: correctness, selection: selection } })

		let correctDistractors = _.filter(questionData.distractors, function(d){ return d.correct })
		let latency = new Date().getTime() - this.state.startTime
		let scormObj = {
			weight: 1,
			id: "knowledgeCheck_"+this.props.componentId,
			type: "fill-in",
			objId: 0, //Not dealing with objectives right now.
			timestamp: new Date().toISOString().slice(11, 22),
			correct_responses: _.map(correctDistractors, function(value, key){return value }),
			learner_response: answeredText,
			result: correctness ? "correct" : "wrong",
			latency: LmsStore.returnFormattedTime(latency),
			description: questionData.questionText
		}
		//Do this later
		//LmsStore.saveSCORMinteraction(scormObj)

		CourseActions.saveInteraction({
			cbSelection: this.state.cbSelection,
			cbCorrect: this.state.cbCorrect,
			cbSelection_text: this.state.cbSelection_text,
			localSelection: this.state.selection,
			tries: this.state.tries,
			id: this.props.componentId,
			userData : userDataUpdate,
			selection: this.state.selection,
		})

		if(this.props.enableNext){
			if(correctness || this.state.tries < 2){
				this.props.enableNext()
			}
		}

		if(questionData.type == "cb" ){
			this.setState({ cbSelection: []});
		}

		this.setState({
			scormObj: scormObj,
			tries: this.state.tries-1,
			localSelection: selection,
			startTime: new Date().getTime(),
			userData: userDataUpdate,
			localCorrectness: correctness,
			feedbackText: correctness ? this.props.feedback.correct.value : this.props.feedback.incorrect.value,
			feedbackOpen: true,
			showIndicator: true,
		}, function(){
			if(this.props.threshold && correctness && !this.props.isAssessment || this.props.threshold && this.state.tries <= 0 && !this.props.isAssessment){
				this.props.getNextThreshold && this.props.getNextThreshold()
			}else if(this.props.threshold && correctness && isLastQuestion && this.props.isAssessment || this.props.threshold && this.state.tries <= 0 && isLastQuestion && this.props.isAssessment){
				this.props.getNextThreshold && this.props.getNextThreshold()
			}
		})
	}

	componentWillReceiveProps = (nextProps) => {
		console.log("Got Props: ", nextProps.selection )
		if(this.props.questionData !== nextProps.questionData){
			this.setState({ showIndicator: false, tries: nextProps.questionData.distractors.length == 2 ? 1 : 2, localSelection: null, localCorrectness: null })
		}
		if(nextProps.selection){
			let cbSelection_text = []
			cbSelection_text = _.map(nextProps.selection, function(selection, index){
				return nextProps.questionData.distractors[selection].value
			})
			this.setState({
				cbSelection: nextProps.selection != null ? nextProps.selection : [],
				cbCorrect: false,
				cbSelection_text: cbSelection_text,
			})
		}else{
			if(this.props.questionData !== nextProps.questionData){
				//console.log("clear it")
				this.setState({
					cbSelection: [],
					cbCorrect: false,
					cbSelection_text: null,
				})
			}
		}
	}
	submitAnswer = ( key ) => {
		console.log('submitAnswer')
		const { cbSelection, tries } = this.state
		const { questionData } = this.props

		const existingKey = _.indexOf(cbSelection, key)
		let newSelection

		if( existingKey != -1 ){
			newSelection = update(cbSelection, {$splice: [[existingKey, 1]] })
		} else {
			console.log('---------------------------------', cbSelection, '---------------------------------')
			newSelection = update(cbSelection, {$push: [key]})
		}

		let ansAry = []
		 _.map(questionData.distractors, function(item, index){
			if( item.correct ){ ansAry.push(index) }
		})

		let cbSelection_text = _.map(newSelection, function(selection, index){
			return questionData.distractors[selection].value
		})

		let correctness = _.isEmpty(_.xor(ansAry, newSelection))
		console.log("SUBMIT ANSWER: ", newSelection)
		this.setState({ cbSelection: newSelection, cbCorrect: correctness, cbSelection_text: cbSelection_text.join(", ") }, () => {
			this.props.answerQuestion ?
				this.props.answerQuestion(
					this.state.cbCorrect,
					this.state.cbSelection,
					this.state.cbSelection_text,
					this.props.questionData)
			: tries == 0 || correctness  ?
					this.props.enableNext(this.state.scormObj ? this.state.scormObj : null)
				: null
		})
	}

	closeFeedback = () => {
	    this.setState({feedbackOpen: false})
  	}

  	getDistractorStyle = (cbSelection, key, type) => {
  		const { questionData, shadowed } = this.props
  		const { tries } = this.state
  		let selected = _.indexOf(cbSelection, key) != -1
  		//console.log("getDistractorStyle: ", cbSelection, key)
  		let correct = _.indexOf(cbSelection, key) != -1 && questionData.distractors[key].correct
  		const styles = {
			distractor: {
				cursor: tries > 0 ? "pointer" : "initial",
				width: "100%",
				minWidth: "340px",
				padding: "10px 10px 15px",
				display: "inline-block",
				border: "2px solid transparent",
			  boxSizing: "border-box",
			},
			distractor_selected: {
				border: "2px solid transparent",
				borderRadius: "2px",
				background: shadowed ? this.props.theme.palette.primary.light : this.props.theme.palette.primary.dark ,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_correct: {
				background: "rgba(35, 154, 35, .6)",
				borderRadius: "2px",
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_incorrect: {
				background: "rgba(179, 18, 18, .6)",
				borderRadius: "2px",
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractorIcon: {
				position: "relative",
				top: "5px",
				fontSize: "20px",
				color: shadowed ? "#fff" : "#ddd" ,
				display: "inline-block",
			},
			selectedIcon: {

			},
			selectedIcon_incorrect: {
				color: shadowed ? "#fff" : "#ddd" ,
			},
			selectedIcon_correct: {
				color: shadowed ? "#fff" : "#ddd" ,
			},

		}

		let styleObj
  		if(selected){
	  		if(this.state.showIndicator){
  				if(correct){
  					type == "distractor" ?
  						styleObj = { ...styles.distractor, ...styles.distractor_selected_correct }
  					:
  						styleObj = { ...styles.distractorIcon, ...styles.selectedIcon_correct }
  				}else{
  					type == "distractor" ?
  						styleObj = { ...styles.distractor, ...styles.distractor_selected_incorrect }
					:
						styleObj = { ...styles.distractorIcon, ...styles.selectedIcon_correct }
  				}
	  		}else{
	  			type == "distractor" ?
	  				styleObj = { ...styles.distractor, ...styles.distractor_selected }
  				:
  					styleObj = { ...styles.distractorIcon, ...styles.selectedIcon }
	  		}
	  	}else{
	  		type == "distractor" ?
	  			styleObj = styles.distractor
  			:
  				styleObj = styles.distractorIcon
	  	}

	  	return styleObj
  	}
 	showCorrect = (distractors) => {
 		let that = this
 		//console.log("SHOWING CORRECT: ", distractors)
 		let corSel = []
 		_.map(distractors, function(d, index){ d.correct ? corSel.push(index) : null })
 		//console.log("CORSEL: ", corSel)

 		if(this.props.questionData.type == "cb"){
	 		that.setState({cbSelection: corSel}, function(){
	 			//console.log("CORRECT STATE: ", this.state.cbSelection)
	 		});
 		}else{
 			that.setState({localSelection: corSel[0]}, function(){
	 			//console.log("CORRECT STATE: ", this.state.localSelection)
	 		});
 		}
 	}
	render() {
		const { threshold, questionData, settings, answerQuestion, correctness, selection, feedback, shadowed, style, outerStyle, isAssessment, isLastQuestion, directions, buttonStyle } = this.props
		const { cbSelection, cbCorrect, cbSelection_text, localSelection, localCorrectness, tries } = this.state
		let submitFunction = this.props.answerQuestion ? this.props.answerQuestion : this.answerQuestion
		let that = this
		const styles = {
			fullWidth: {
				width: "100%",
			},
			assessmentHolder: {
				minHeight: "calc( 100vh - 120px )",
			},
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			questionText: {
				fontSize: "24px",
				marginBottom: "10px",
		    	lineHeight: "30px",
		    	width: "100%",
		    	fontFamily: "Roboto",
		    	fontWeight: 300,
			},
			distractor: {
				cursor: tries > 0 ? "pointer" : "initial",
				width: "100%",
				minWidth: "340px",
				padding: "10px 10px 15px",
				display: "inline-block",
				border: "2px solid transparent",
			    boxSizing: "border-box",
			    fontFamily: "Roboto",
			    fontWeight: 300,
			},
			distractor_selected: {
				border: "2px solid transparent",
				borderRadius: "2px",
				background: shadowed ? this.props.theme.palette.primary.light : this.props.theme.palette.primary.dark,
				color: shadowed ? "#fff" : "#ddd" ,
			},
			distractor_selected_correct: {
				background: "rgba(35, 154, 35, 1)",
				borderRadius: "2px",
				color: "#fff",
			},
			distractor_selected_incorrect: {
				background: "#c52a0c",
				borderRadius: "2px",
				color: "#fff",
			},
			distractorIcon: {
				position: "relative",
				top: "4px",
				fontSize: "20px",
				color: shadowed ? "#fff" : "#666" ,
				display: "inline-block",
			},
			selectedIcon: {

			},
			selectedIcon_incorrect: {
				color: shadowed ? "#fff" : "#fff" ,
			},
			selectedIcon_correct: {
				color: shadowed ? "#fff" : "#fff" ,
			},
			distractorText: {
				display: "inline",
				paddingLeft: "10px",
			    marginLeft: "-30px",
			    position: "relative",
			    left: "30px",
			},
			feedback: {
				padding: "20px",
				margin: "40px 0",
				width: "100%",
			},
			questionHold: {
				minHeight: "400px",
				width: "100%",
			    margin: "0 auto 45px auto",
			    marginBottom: 0,
			},
			title: {
				fontFamily: "Rajdhani",
				fontWeight: 700,
				paddingBottom: "15px",
				textTransform: "uppercase",
				letterSpacing: "0.2rem",
			},
			feedbackContainer: {
				zIndex: "9999999",
			},
			feedbackText: {
				color: "#000",
				fontWeight: "300",
				fontFamily: "Roboto",
				fontSize: "1.2rem",
 			},
 			feedbackTitle: {
 				textTransform: "uppercase",
 			},
 			feedbackCloseBtn: {
 				color: "#00258f",
 			},
 			button: {
				backgroundColor: '#fff',
				color: '#0d1017',
			    border: '1px solid #fff',
			    textTransform: 'uppercase',
			    font: '1.3rem/2.9rem "Rajdhani", Arial, sans-serif',
			    fontSize: '.9rem',
			    letterSpacing: '.09em',
		        margin: 10,
			    padding: '0 2.6rem',
		        fontSize: '1.3rem',
		        fontWeight: 700
			},
			disabled: {
				opacity: 0.2
			},
		}




		let sel = localSelection != null ? localSelection : selection
		let cor = correctness != null ? correctness : localCorrectness
		let cbSel_var = cbSelection

		//console.log('cor',cor)

		//console.log("SELECTION:", sel)

		let feedbackText = feedback
		let feedbackLabel = sel != null && cor ? 'Correct!' : 'Incorrect.'
		//console.log('cor',cor)

		if(typeof(feedbackText) == "object" && feedback != null){
			sel != null && cor ?
				feedbackText = feedback.correct.value
			:
				sel != null && tries > 0 ?
					feedbackText = feedback.tryagain && feedback.tryagain.value ? feedback.tryagain.value : null
				: sel != null && tries <= 0 ?
					feedbackText = feedback.incorrect.value
				: feedbackText = null
		}
		//console.log('cor',cor)

		if(!correctness && this.state.tries == 0 && this.props.settings.showFeedback ){
			sel = []
			cbSel_var = []
			_.map(questionData.distractors, function(d, index){
				d.correct ? cbSel_var.push(index) : null
			})
			if(questionData.type != "cb"){
				sel = cbSel_var[0]
			}
			cor = true
		}
		//console.log('cor',cor)
		let distractors =  _.map(questionData.distractors, function(option, key){
			if(questionData.type != "cb"){
				return 	[<div
							style={
								sel == key && cor && settings.showFeedback ?
									{ ...styles.distractor, ...styles.distractor_selected_correct }
								:
									sel == key && !cor && settings.showFeedback ?
										{ ...styles.distractor, ...styles.distractor_selected_incorrect }
									:
										sel == key ?
											{ ...styles.distractor, ...styles.distractor_selected }
										:
											styles.distractor
									 }
							key={ "dis_"+ key }
							onClick={ tries > 0 && !cor || that.props.answerQuestion ? submitFunction.bind(that, option.correct, key, option.value, questionData) : null }>
							<span >
								<Icon className="material-icons"
									style={
										sel == key && cor && settings.showFeedback ?
											{ ...styles.distractorIcon, ...styles.selectedIcon_correct }
										:
											sel == key && !cor && settings.showFeedback ?
												{ ...styles.distractorIcon, ...styles.selectedIcon_incorrect }
											:
												sel == key ?
													{ ...styles.distractorIcon, ...styles.selectedIcon }
												:
													styles.distractorIcon
											 } >
										{ sel == key ? "radio_button_checked" : "radio_button_unchecked" }
								</Icon>
							</span>
							<div style={ styles.distractorText }>
								{ option.value }
							</div>
						</div>, <br />]
			}else{
				//console.log(cbSelection, key, _.indexOf(cbSelection, key) )
				return 	[<div style={ that.getDistractorStyle(cbSel_var, key, "distractor")}
							key={ "dis_"+ key }
							onClick={ tries > 0 || that.props.answerQuestion ? that.submitAnswer.bind(that, key) : null  }>
							<span >
								<Icon className="material-icons"
									style={ that.getDistractorStyle(cbSel_var, key, "icon")} >
										{ _.indexOf(cbSel_var, key) != -1 ? "check_box" : "check_box_outline_blank"  }
								</Icon>
							</span>
							<div style={styles.distractorText}>
								{ option.value }
							</div>
						</div>, <br />]
			}
		})

		let rootClass = settings.knowledgeCheck ? "container" : null
		return (
			<div style={ outerStyle ? {width: '100%', ...outerStyle} : {width: '100%'} }>
				<div class={rootClass} style={style ? {...styles.questionHold, ...style} : styles.questionHold}>

					<div class="mdl-grid">
						{ settings.knowledgeCheck && !isAssessment && <h2 style={ styles.title }>Knowledge Check</h2> }
						<div class="mdl-cell mdl-cell--12-col">
							<div style={styles.questionText}>{ questionData.questionText } </div>
						</div>
						<div class="mdl-cell mdl-cell--6-col">
							{ distractors }
						</div>
						<div class="mdl-cell mdl-cell--6-col">
							{ questionData.assoc_img ? <img style={{maxWidth: "100%", width: "100%"}} src={questionData.assoc_img} /> : null }
						</div>
						{ feedback && feedbackText && settings.showFeedback ?
							<Dialog style={ styles.feedbackContainer } onClose={this.handleClose} aria-labelledby="simple-dialog-title" open={this.state.feedbackOpen}>
					        	<DialogTitle style={ styles.feedbackTitle } id="simple-dialog-title">{ feedbackLabel }</DialogTitle>
					        	<DialogContent>
					            	<DialogContentText style={ styles.feedbackText } id="alert-dialog-description">
					              		{ feedbackText }
					            	</DialogContentText>
					        	</DialogContent>
					        	<DialogActions>
					            	<Button style={ styles.feedbackCloseBtn } onClick={this.closeFeedback} color="primary">
					              		Close
					            	</Button>
					        	</DialogActions>
					    	</Dialog>
					    : null }
					</div>

					{ questionData.type == "cb" && !isAssessment || questionData.type == "cb" && settings.knowledgeCheck && tries > 0 ?
						<div><ButtonBase style={ this.state.cbSelection.length > 0 ? buttonStyle : { ...buttonStyle, ...styles.disabled } } disabled={ this.state.cbSelection.length > 0 ? false : true } onClick={ submitFunction.bind(this, cbCorrect, cbSelection, cbSelection_text, questionData)}>Submit Answer</ButtonBase></div>
					: null }
				</div>
				{settings.knowledgeCheck && tries > 0 && !cor && this.props.settings.thresholdDirections && threshold ?
					<InteractionBumper style={{ background: "#c74c4c", color: "#fff" }} text={this.props.settings.thresholdDirections} />
					: null}
			</div>
		)
	}
}

export default withTheme()(Question);
