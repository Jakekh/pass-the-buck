var React = require('react')

import { withTheme } from '@material-ui/core/styles'
import * as CourseActions from '../../actions/CourseActions'

class Stage extends React.Component {
    constructor(){
        super()
    }
    componentDidMount(){
        this.props.callback()
    }
    render() {
        const { scenes } = this.props

        return (
            <div>
                { scenes }
            </div>
        )           
    }
}
export default withTheme()(Stage)
