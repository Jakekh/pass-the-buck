var React = require('react')
var $ = require('jquery')
import _ from 'lodash'
import ReactDOM from 'react-dom'

import Button from '@material-ui/core/Button'
import ButtonBase from '@material-ui/core/ButtonBase'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Paper from '@material-ui/core/Paper'
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import GridListTileBar from '@material-ui/core/GridListTileBar'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

import { withTheme } from '@material-ui/core/styles'
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import MsgStore from '../../stores/MsgStore'
import ModalStore from '../../stores/ModalStore'

class ClickandDiscover extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined){
			this.state = {
				popupOpen: false,
				selectedItemIndex: savedState.si,
				selectedItem: props.pairs[savedState.si],
				completed: savedState.c,
				svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}else{
			this.state = {
				popupOpen: false,
				completed: Array(props.pairs.length).fill(0),
				svg: props.type == 'svg' ? <object id="svg-items" type="image/svg+xml" data={ this.props.svgUrl } /> : null
			}
		}
	}

	componentDidMount() {
		const that = this
		const { pairs, type } = this.props
		switch(type){
			case 'svg' :
				_.map(pairs, (value, key) => {
					const svg = document.getElementById('svg-items')
					svg.addEventListener("load", () => {
						const svgDoc = svg.contentDocument
						const svgItem = svgDoc.getElementById(value.item)
						const checkInd = svgDoc.getElementById(`check_${key}`)
						$(svgItem).css('cursor', 'pointer')
						$(svgItem).on('click', () => {
							$(checkInd).css('display', 'block')
							that.selectItem(key)
						})
						this.state.completed[key] == 1 ? $(checkInd).css('display', 'block') : null
					})
				})
			break
			case 'launchable' :

			break
			case 'terms' : 

			break
		}	
	}

	openPopup = () => {
		this.setState({popupOpen: true})
	}

	closePopup = () => {
		this.setState({popupOpen: false})
	}

	selectItem = (index) => {
		const that = this
		const item = this.props.pairs[index]
		const outId = this.state.selectedItem ? this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defId = item.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${outId}`).removeClass('expanded').addClass('collapsed')

		let newAry = this.state.completed
		newAry[index] = 1
		
		this.setState({
			selectedItemIndex: index,
			selectedItem: item,
			completed: newAry
		}, function(){
			switch(this.props.type){
				case "svg" :
				case "terms" :
				break
				case "terms-modal" :
					this.openPopup()
				break
				case "images-full" :
				case "images" :
					$(`#${defId}`).removeClass('expanded').addClass('collapsed')
					setTimeout(() => {
						$(`#${defId}`).removeClass('collapsed').addClass('expanded')
					}, 200)
					$('.active').removeClass('active')
					$(`#imageBtn_${this.props.componentId}_${index}`).addClass('active')
				break
				case "images-modal" :
					this.openPopup()
				break
				case "launchable" :
					window.open(  `${ item.url }` , '_blank')
				break

			}
			CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItemIndex, c: that.state.completed})
		})
	}
	closeDef = () => {
		const that = this
		const defId = this.state.selectedItem.item.replace(/\W+/g, '-').toLowerCase() 
		$(`#${defId}`).removeClass('expanded').addClass('collapsed')
		setTimeout(function(){
			that.setState({
				selectedItem: null
			}, function(){
				CourseActions.saveInteraction({id: that.props.componentId, si: that.state.selectedItem, c: that.state.completed})
			})
		}, 1000)
	}
	render() {
		const{ title, directions, pairs, imgHeight, type, componentId, icon, imgClass } = this.props
		const { selectedItem, completed, svg, popupOpen } = this.state
		const styles = {
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			interactionCont:{
				transition: "all 500ms ease-in",
			},
			shadow: {
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    color: "#898989",
			    background: "rgb(236, 236, 236)",
			},
			inner: {
				minWidth: "100%",
			    color: "#898989",
		        margin: '0 auto',
			    boxShadow: "0 0 100px rgba(128, 128, 128, 0.3) inset",
			    background: "rgb(236, 236, 236)",
			    marginTop: 60,
			},
			interactiveIcon: {
				fontSize: "80px",
				color: "#898989",
			},
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: this.props.theme.palette.primary.dark,
				display: "inline-block",
				color: "#fff",
				width: '100%',
				boxSizing: 'border-box',
			},
			svgBoxCont: {
				padding: "0 5% 0 0",
			},
			svgDefBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: this.props.theme.palette.primary.dark,
				marginTop: "20%",
				color: "#fff",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			imagesCont: {
				display: 'flex',
			    flexWrap: 'wrap',
			    justifyContent: 'space-around',
			    overflow: 'hidden',
			    backgroundColor: this.props.theme.palette.background.paper,
			},
			imagesCont: {
				display: 'flex',
			    flexWrap: 'nowrap',
			    justifyContent: 'space-around',
			    overflow: 'hidden',
			    backgroundColor: this.props.theme.palette.background.paper,
			},
			gridList: {
				flexWrap: 'nowrap',
			    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
			    transform: 'translateZ(0)',
			    width: '100%',
			},
			titleStyle: {
				color: 'rgb(0, 188, 212)',
			},
			respImg: {
				cursor: "pointer",
				maxWidth: "100%",
				width: "auto",
			},
			icon: {
				color: "#fff",
			},
			termIcon: {
				color: "#0d1017",
				marginRight: 10,
			},
			title: {
				fontWeight: "bold",
				textShadow: "none",
				background: "none",
				opacity: .8,
			},
			imageBtn: {
			    position: 'absolute',
			    left: 0,
			    top: 0,
			    width: '100%',
			    height: '100%',
			},
			imageBtnHover: {
			    position: 'absolute',
			    left: 0,
			    top: 0,
			    width: '100%',
			    height: '100%',
			    backgroundColor: 'rgba(43, 89, 130, .5)',
			},
			termsCont: {
			    textAlign: 'left',
			    padding: '0 5.75rem'
			},
			term: {
				backgroundColor: '#fff',
				color: '#0d1017',
			    border: '1px solid #fff',
			    textTransform: 'uppercase',
			    font: '1.3rem/2.9rem "Rajdhani", Arial, sans-serif',
			    fontSize: '.9rem',
			    letterSpacing: '.09em',
		        margin: 10,
			    padding: '0 2.6rem',
		        fontSize: '1.3rem',
		        fontWeight: 700,
			},
			modal: {
				color: "#000",
				fontWeight: "300",
				fontFamily: "Roboto",
				fontSize: "1.2rem",
			},
		}

		const that = this
		let children = []
		switch(type){
			case 'terms' :
				children = _.map(pairs, (value, key) => {
					return <Button key={ `btn_${componentId}_${key}` } onClick={ that.selectItem.bind(that, key) } primary={ true } style={ styles.button } icon={ completed[key] == 1 && <Icon>check_circle</Icon> } >{ value.item }</Button>
				})
			break
			case 'terms-modal' :
				children = _.map(pairs, (value, key) => {
					return <ButtonBase key={ `btn_${componentId}_${key}` } onClick={ that.selectItem.bind(that, key) } style={ styles.term }>{ completed[key] == 1 && <Icon style={ styles.termIcon }>check_circle</Icon> }{ value.item }</ButtonBase>
				})
			break
			case 'images' :
				children = _.map(pairs, (value, key) => {
					return <GridListTile key={ `GridList_${componentId}_${key}` }>
					            <img src={ value.item } />
				          		<ButtonBase id={ `imageBtn_${componentId}_${key}` } class='imageBtn' style={ styles.imageBtnHover } onClick={that.selectItem.bind(that, key)}></ButtonBase>
					            <GridListTileBar style={ styles.title } title={ value.title } actionIcon={ completed[key] == 1 ? <Icon style={ styles.icon }>check_circle</Icon> : null } ></GridListTileBar>
					        </GridListTile>
				})
			break
			case 'images-modal' :
				children = _.map(pairs, (value, key) => {
					return <GridListTile key={ `GridList_${componentId}_${key}` }>
					            <img id={ `imageBtn_${componentId}_${key}` } src={ value.item } onClick={that.selectItem.bind(that, key) } />
					            <GridListTileBar style={ styles.title } title={ value.title } actionIcon={ completed[key] == 1 ? <Icon style={ styles.icon }>check_circle</Icon> : null } ></GridListTileBar>
					        </GridListTile>
				})
			break
			case 'images-full' :
				children = _.map(pairs, (value, key) => {
					return <div key={ `img_${componentId}_${key}` } style={ {display: "inline-block"} }><img onClick={ that.selectItem.bind(that, key) } key={ key } src={ value.item } style={ styles.respImg } /></div>
				})
			break
			case 'launchable':
				children = _.map(pairs, (value, key) => {
					return <Button key={ `Btn_${componentId}_${key}` } onClick={ that.selectItem.bind(that, key) } primary={ true } style={ styles.button } icon={ completed[key] == 1 ? <Icon>check_circle</Icon> : null }>{ value.title }</Button>
				})
			break
			case 'svg':

			break
		}

		const defId = selectedItem ? selectedItem.item.replace(/\W+/g, '-').toLowerCase() : null
		const defBox = selectedItem && type != "launchable" && type != "terms-modal" && type != "images-modal" ? <div id={defId}>
										<Paper style={type == 'svg' ? styles.svgDefBox : styles.defBox} key={selectedItem.item}>
											<IconButton onClick={this.closeDef} style={styles.closeDefIcon} tooltip="Close">
									     		<Icon className="material-icons" style={styles.icon} >close</Icon>
										    </IconButton>
											<h3 style={styles.defTitle}>{type == 'svg' || type == 'images' || type == 'images-full' ? selectedItem.title : selectedItem.item}</h3>
											<div style={styles.defBoxContent}>
												{ ReactHtmlParser(selectedItem.def) }
											</div>
										</Paper>
									</div> : null 

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		return (
			<div style={ title ? this.props.innerChild ? styles.inner : styles.shadow : null }>
				<div style={ styles.interactionCont } class={ gridClass } key={ componentId }>
					<div style={ styles.centeredContent }>
						{ 
							// icon ? <div class="golfBall"><Icon className="material-icons" >{icon}</Icon></div> : null
						}
						{ title ? <h2 >{title}</h2> : null }
						{ directions ? <h4 >{directions}</h4> : null }
						{ type == 'svg' ? 
							<div class={gridClass} key={"div_0"}>
								<div class="mdl-cell mdl-cell--8-col" style={ styles.svgBoxCont } key={ "col_0" }>{ svg }</div>
								<div class="mdl-cell mdl-cell--4-col" key={ "col_1" }>{ defBox }</div>
							</div>
						:
							<div key={"div_1"}>
								{ type == "images" || type == "images-modal" ?
									<div class={imgClass != undefined ? imgClass : null}>
										<div style={styles.imagesCont}>
	    									<GridList style={styles.gridList} cellHeight={ imgHeight } cols={ pairs.length }>
	    										{ children }
	    									</GridList>
	  									</div>
										{ defBox ? defBox : <div>&nbsp;</div> }
  									</div>
								: null }
								{ type == "images-full" ? 
									this.props.sideBySide ? 
										<div class="mdl-grid">
											<div class="mdl-cell mdl-cell--8-col" key={"col_0"}>
												{ children }
											</div>
											<div class="mdl-cell mdl-cell--4-col" key={"col_1"}>
												{ defBox ? defBox : <div>&nbsp;</div> }
											</div>
										</div>
									:
										<div style={styles.root}>
											{ children } { defBox ? defBox : <div>&nbsp;</div> }
										</div>
    									
								: type == "terms-modal" ? 
									<div style={ styles.termsCont }>{ children }</div> 
								: null}
								
							</div>
						}
						{ selectedItem ?
							<Dialog style={ {zIndex: "9999999"} } onClose={ this.handleClose } aria-labelledby="simple-dialog-title" open={ popupOpen }>
					        	<DialogTitle id="simple-dialog-title">{ selectedItem.title }</DialogTitle>
					        	<DialogContent>
					            	<DialogContentText id="alert-dialog-description" style={ styles.modal }>
					              		{ ReactHtmlParser(selectedItem.def) }
					            	</DialogContentText>
					        	</DialogContent>
					        	<DialogActions>
					            	<Button onClick={ this.closePopup } color="primary">
					              		Close
					            	</Button>
					        	</DialogActions>
					    	</Dialog>
				    	: null }
					</div>
				</div>
			</div>
		)	
	}
}

export default withTheme()(ClickandDiscover)
