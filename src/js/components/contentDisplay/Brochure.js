var React = require('react')
var $ = require('jquery')

require("../../../js/turn_js/lib/turn.js")

import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import ReactHtmlParser from "react-html-parser"
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'

export default class Brochure extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		console.log(savedState)
		if(savedState != undefined){
			this.state = {
				completed: savedState.c
			}
		}else{
			this.state = {
				completed: Array(props.pages.length).fill(0),
			}
		}
	}
	getSize = () => {
	  
	  var width = $('.container').width();
	  var aspRat
	  width < 840 ?
	 	 aspRat = 828 / 640 
	  :
	 	 aspRat = 828 / 1280 
	  var height = $('.container').width() * aspRat;
	  console.log('get size', width, height);

	  return {
	    width: width,
	    height: height
	  }
	}

	resize = () => {
	  var size = this.getSize();
	  if (size.width > 840) { // landscape
	    $('.flipbook').turn('display', 'double');
	  }
	  else {
	    $('.flipbook').turn('display', 'single');
	  }
	  $('.flipbook').turn('size', size.width, size.height);
	}
	componentDidMount() {

		window.addEventListener('resize', this.resize);
		var size = this.getSize();

		setTimeout(function(){
			console.log("INIT")
			$('.flipbook').turn({
				// Width
				display: size.width > 840 ? "double" : "single",

				width: size.width,
				
				// Height

				height: size.height,

				// Elevation

				elevation: 50,
				
				// Enable gradients

				gradients: true,
				
				// Auto center this flipbook

				autoCenter: true

			})}, 100)
	}
	
	render() {
		const{ pages, shadowed } = this.props
		const { completed } = this.state
		const styles = {
			brochureRoot: {
				padding: "20px"
			},
		}

		const that = this

		const children = _.map(pages, (value, key) => {
			return <div key={"brochureDiv_"+ key} style={ { backgroundImage: `url(${value})`, backgroundSize: "contain" } }></div>
			/*return <div key={"brochureDiv_"+ key} style={styles.page}><img src={value} /> </div>*/
		})

		return (
			<div class={ shadowed ? "shadowSection" : null } style={styles.brochureRoot} >
				<div class="flipbook-viewport">
					<div class="container">
						<div class="flipbook">
							{ children }
	    					<div class="hard"></div>
						</div>
					</div>
				</div>
			</div>
		)	
	}
}