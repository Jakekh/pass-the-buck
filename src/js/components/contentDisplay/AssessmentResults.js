var React = require('react')
var $ = require('jquery')

import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import ReactHtmlParser from "react-html-parser"
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CourseStore from '../../stores/CourseStore'

export default class AssessmentResults extends React.Component {

	printReview = () => {
		var printContents = new $("#assessment_rem").clone();
      var myWindow = window.open("", "popup", "width=1000,height=600,scrollbars=yes,resizable=yes," +
          "toolbar=no,directories=no,location=no,menubar=no,status=no,left=0,top=0")
      var doc = myWindow.document
      doc.open()
      doc.write("<!DOCTYPE \">")
	    doc.write("<html>")
	    doc.write("<head>")
	    doc.write("<link href='../../src/css/print.css' rel='stylesheet' type='text/css' />")
	    doc.write("</head>")
	    doc.write("<body onload='window.print()'>")
	    doc.write("<h2>"+CourseStore.courseData.title+"</h2>")
	    doc.write("<h3>Your Score: "+ this.props.score+"%</h3>")
	    doc.write($(printContents).html())
	    doc.write("</body>")
	    doc.write("</html>")
	    doc.close()
      setTimeout(function(){doc.close()},10);
	}
	render() {
		const { score, postAssessment, retry, exit, missedQuestions, mode } = this.props
		let that = this
		const styles = {
			fullWidth: {
				width: "100%",
			},
			feedback: {
				padding: 15
			}
		}
		return (
			<div class="mdl-grid" style={styles.fullWidth}>
				<div key={"rem_0"} class="mdl-cell mdl-cell--4-col">
					<h3>How did you do? </h3>
					<h4>Your Score: <b>{ score }%</b></h4>
					{ ReactHtmlParser(postAssessment.text) }
					{mode == "testout" ?
						<div>
							{score < 80 ?
								<p>You did not pass, you must take the course from the beginning.</p>
							:
								<p>Congratulations you passed! You have tested out of this course! You may now exit the course.</p>
							}
							{score < 80 ?
								<Button color={"primary"} variant={"raised"} onClick={CourseStore.exitTestout} primary={true} >Restart Course</Button>
							:
								<Button color={"primary"} variant={"raised"} onClick={exit} primary={true} >Exit</Button>
							}
						</div>
					:
						<div>
							{score < 80 ? <p>You did not pass, please retry the assessment.</p> : <p>Congratulations you passed!</p>}
							{score < 80 ? <Button color={"primary"} variant={"raised"} onClick={retry} primary={true} >Retry</Button> : <Button color={"primary"} variant={"raised"} onClick={exit} primary={true} >Exit</Button>}
						</div>
				}
				</div>
				<div key={"rem_1"} class="mdl-cell mdl-cell--8-col">
					{ missedQuestions && missedQuestions.length ?
						<Paper >
							<Toolbar noGutter={false}>
								<Typography style={{flex: 1}}>Missed Questions</Typography>
								<Button color={"primary"} variant={"raised"} onClick={this.printReview.bind(this)}>Print</Button>
							</Toolbar>
							<div style={styles.feedback} id="assessment_rem">{ missedQuestions }</div>
						</Paper>
					: null }
				</div>
			</div>
			)
		}
}
