var React = require('react')
var $ = require('jquery')

import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Paper from '@material-ui/core/Paper'
import ReactHtmlParser from "react-html-parser"
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import InteractionBumper from '../UI/InteractionBumper'
import AssessmentReview from '../contentDisplay/AssessmentReview'
import update from 'immutability-helper'
import Question from '../contentDisplay/Question'
import Simulation from '../contentDisplay/Simulation'
import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import CourseStore from '../../stores/CourseStore'
import MsgStore from '../../stores/MsgStore'
import AssessmentResults from "../contentDisplay/AssessmentResults"
import Divider from '@material-ui/core/Divider'
import Chip from '@material-ui/core/Chip'

import { withTheme } from '@material-ui/core/styles';

class Assessment extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		console.log(savedState)
		if(savedState != undefined){
			this.state = {
				score: savedState.score,
				postAssessment: props.PostAssessment,
				questions: savedState.questions,
				currentQuestion: savedState.currentQuestion,
				userData : savedState.userData,
				assessmentComplete: savedState.assessmentComplete,
				scorm_interaction: savedState.scorm_interaction,
				assessmentStarted: savedState.assessmentStarted,
				startTime: savedState.startTime,
				settings: props.settings,
				assessmentReview: savedState.assessmentReview,
				scormInts: savedState.scormInts,
				selection: savedState.selection,
				activeQuestion: savedState.activeQuestion,
				questionSelections: savedState.questionSelections,
				nextThresholdLoaded: false
			}
		}else{
			this.state = {
				postAssessment: props.PostAssessment,
				questions: null,
				currentQuestion: 0,
				userData : [],
				scorm_interaction: [],
				assessmentStarted: false,
				startTime: null,
				settings: props.settings,
				assessmentReview: false,
				scormInts: [],
				selection: null,
				activeQuestion: null,
				questionSelections: [],
				score: null,
				nextThresholdLoaded: false
			}
		}
	}

	componentDidMount() {
		CourseStore.on('questions_generated', this.setQuestions)
		!this.state.questions ?
			CourseStore.generateQuestions(this.props.questionPools)
		: null
	}

	componentWillUnmount() {
		CourseStore.removeListener('questions_generated', this.setQuestions)
	}
	exit = () => {
		MsgStore.createMsg({title: "Exit", message: "Are you sure you want to exit the course?", type: 'prompt', submit: LmsStore.saveSuspendData.bind(this, 'exit')})
	}
	setQuestions = () => {
		this.setState({
			questions: CourseStore.returnQuestions()
		})
	}

	answerQuestion(correctness, selection, answeredText, question){
		let that = this
		const { questions, currentQuestion, userData, scormInts, questionSelections } = this.state

		let userDataUpdate = update(userData, { [currentQuestion] : {$set:  { correctness: correctness, selection: selection } } })

		let correctDistractors = _.find(questions[currentQuestion].distractors, function(d){ return d.correct})
		let latency = new Date().getTime() - this.state.startTime
		let scormObj = {
			weight: 1,
			id: `assessment_${this.props.componentId}_${currentQuestion}`,
			type: "fill-in",
			objId: 0, //Not dealing with objectives right now.
			timestamp: new Date().toISOString().slice(11, 22),
			correct_responses: _.map(correctDistractors, function(value, key){return value }),
			learner_response: answeredText,
			result: correctness ? "correct" : "wrong",
			latency: LmsStore.returnFormattedTime(latency),
			description: questions[currentQuestion].questionText
		}

		LmsStore.saveSCORMinteraction(scormObj)

		let questionPos = _.findIndex(scormInts, function(o) { return o.id == `assessment_${that.props.componentId}_${currentQuestion}` })
		let scormIntsUpdate

		questionPos == "-1" ?
			scormIntsUpdate = update(scormInts, { $push:  [scormObj] })
		:
			scormIntsUpdate = update(scormInts, { [ questionPos ] : {$set:  scormObj} })

		this.setState({
			startTime: new Date().getTime(),
			userData: userDataUpdate,
			correctness: correctness,
			feedback: correctness ? questions[currentQuestion].feedback.correct.value : questions[currentQuestion].feedback.incorrect.value,
			scormInts: scormIntsUpdate,
		}, () => {
			this.saveInteraction()
		})
	}
	saveInteraction(){
		CourseActions.saveInteraction({
			id: this.props.componentId,
			score: this.state.score,
			questions: this.state.questions,
			currentQuestion: this.state.currentQuestion,
			userData : this.state.userData,
			scorm_interaction: this.state.scorm_interaction,
			assessmentStarted: this.state.assessmentStarted,
			assessmentComplete: this.state.assessmentComplete,
			startTime: this.state.startTime,
			assessmentReview: this.state.assessmentReview,
			scormInts: this.state.scormInts,
			selection: this.state.selection,
			activeQuestion: this.state.activeQuestion,
			questionSelections: this.state.questionSelections
		})
	}
	reviewQuestion = (data, currentQuestion) => {
		console.log(data)
		const { questions, userData } = this.state
		console.log(currentQuestion)
		this.setState({
			activeQuestion: true,
			currentQuestion: currentQuestion,
			selection: userData[currentQuestion].selection,
		})
	}
	nextQuestion = () => {
		const { questions, currentQuestion, activeQuestion, showNext } = this.state

		if(showNext){
			this.setState({ showNext: false })
		}
		if(activeQuestion){
			this.setState({
				activeQuestion: false,
				selection: null,
				feedback: null,
				correctness: null,
			})
		}
		if(questions.length - 1 > currentQuestion){
			this.setState({
				currentQuestion: currentQuestion + 1,
				selection: null,
				feedback: null,
				correctness: null,
			}, () => {
				this.saveInteraction()
			})
		}else{
			if(this.state.settings.review_before_submit){
				this.setState({
					assessmentReview: true,
					selection: null,
					feedback: null,
					correctness: null,
				}, () => {
					this.saveInteraction()
				})
			}else{
				this.completeAssessment()
			}
		}
	}
	beginAssessment = () => {// add auto scroll
		this.setState({
			assessmentStarted: true,
			startTime: new Date().getTime(),
		}, () => {
			this.saveInteraction()
		})
		//$("html, body").off().stop().animate({ scrollTop: $(`#assesmentCont_${this.props.componentId}`).offset().top - 48}, 500, "swing")
	}
	completeAssessment = () => {
		this.setState({
			assessmentReview: false,
			activeQuestion: false,
			selection: null,
			feedback: null,
			assessmentComplete: true,
		}, () => {
			this.processAssessment()
		})
	}
	retry = () => {
		this.setState({
			score: null,
			currentQuestion: 0,
			userData : [],
			assessmentStarted: false,
			startTime: null,
			assessmentComplete: false,
		}, () => {
			this.saveInteraction()
		})
	}
	processAssessment = () => {
		const { userData, questions } = this.state
		let numCorrect = 0

		_.map(userData, function(item, key){
			if(item.correctness){
				numCorrect++
				console.log(numCorrect)
			}
		})

		let score =  numCorrect / questions.length * 100
		LmsStore.setScore(score)
		if(score > this.props.masteryScore){
			this.props.getNextThreshold()
		}
		this.setState({
			score: score
		}, () => {
			this.saveInteraction()
		})
	}
	getMissedQuestions = () => {
		const { userData, score, questions, assessmentComplete } = this.state

		if(assessmentComplete && score < 100){

			let missedQuestions = []
			let courseData = CourseStore.returnCourseData()
			_.map(questions, function(val, key){
				console.log("Question Missed? ", userData[key] )
				if(userData[key].correctness != true){
					missedQuestions.push(val)
				}
			})

			let grouped = _.groupBy(missedQuestions, 'reviewContent')
			console.log(courseData.modules)

			let reviewList = _.map(grouped, function(section, key){
				let modSecKeys = key.split(',')

				let secQuestions = _.map(section, function(question, key){
					return <div key={`qText_${key}`}><p style={{padding: "0 10px", margin: "10px 0"}}><b>Question:</b> {question.questionText} </p><Divider /></div>
				})

				let questionReviewText
				courseData.modules[parseInt(modSecKeys[0])] ?
					questionReviewText =  <div>
							<h5>{ `${ courseData.modules[parseInt(modSecKeys[0])].title } ${courseData.modules[parseInt(modSecKeys[0])].sections[parseInt(modSecKeys[1])].Section.title}`  }</h5>
							{ secQuestions }
						</div>
				:
				questionReviewText = <p>No Review content Found</p>
				return questionReviewText
			})
			CourseActions.saveAssessmentRem(reviewList)
			return reviewList
		}
	}

	nextThreshold = () => {
		this.setState({ nextThresholdLoaded: true })
		this.props.getNextThreshold()
	}

	enableNext = () => {
		this.setState({ showNext: true })
	}

	render() {
		const { threshold, title, componentId, intro, thresholdDirections } = this.props
		const { nextThresholdLoaded, activeQuestion, userData, scormInts, score, settings, questions, assessmentStarted, assessmentReview, currentQuestion, feedback, correctness, parMeter, assessmentComplete, postAssessment, completeSectionDirections } = this.state

		console.log("Assessment Loading: ", settings)
		let that = this
		const styles = {
			fullWidth: {
				width: "100%",
			},
			assessmentHolder: {
				minHeight: "calc( 100vh - 120px )",
				color: this.props.theme.palette.primary.main,
				backgroundColor: "#fff",
			},
			KCHolder: {
				color: '#fff',
				backgroundColor: this.props.theme.palette.primary.main,
				paddingBottom: 50,
			},
			centeredContent: {
				width: "100%",
				textAlign: "center",
			},
			button: {
				margin: "20px 10px",
			},
			defBox: {
				position: "relative",
				maxWidth: "1200px",
				textAlign:"left",
				padding: "20px",
				margin: "auto",
				background: "#fff",
			},
			closeDefIcon: {
				position: "absolute",
				right: "10px",
				top: "10px",
			},
			defTitle: {
				marginTop: "0px",
			},
			footer: {
				position: "absolute",
				bottom: 0,
				left: 0,
				width: "100%",
				background: "#fff"
			},
			footerText: {
				padding: 20
			},
			chip: {
				backgroundColor: settings.knowledgeCheck ? '#fff' : this.props.theme.palette.primary.main,
				color: settings.knowledgeCheck ? this.props.theme.palette.primary.main : "#fff",
			},
			chipCont: {
				width: '100%',
			},
		}

		let questionData = null
		let scoreValues = []
		let scoreValue = []
		let scoreDisplay = null
		let itemStyles = null

		let missedQuestions = this.getMissedQuestions()

		let gridClass = "container mdl-grid"
		!this.props.columnSpacing ? gridClass += " mdl-grid--no-spacing" : null

		if(userData[currentQuestion]){
			console.log(userData[currentQuestion].selection)
		}

		return (
			questions ?
				<div id={ `assesmentCont_${componentId}` } key={ componentId }>
					<div class={ settings.knowledgeCheck ? "" : "shadowSection" } style={settings.knowledgeCheck ? styles.KCHolder : styles.assessmentHolder}>
						<div class={gridClass}>

							<div style={styles.centeredContent}>
								<h2 style={CourseStore.theme.titles}>{this.props.title}</h2>
							</div>

							{ assessmentStarted && !assessmentComplete && !assessmentReview || activeQuestion ?
								<div style={styles.chipCont}>
									<Chip style={styles.chip} label={`Question ${ currentQuestion + 1 } of ${ questions.length }`} />
									{ !settings.knowledgeCheck && <h5 style={styles.directions}>{questions[currentQuestion].directions}</h5> }
									{ !settings.knowledgeCheck && <Divider /> }
									<br />
								</div>
							: null }

							{ !assessmentStarted ?
								intro ?
									<div style={styles.centeredContent}>
										<h4>{ intro }</h4>
										<Button color={"primary"} variant={"raised"} onClick={this.beginAssessment}>Begin</Button>
									</div>
								: this.beginAssessment()
							:
								assessmentReview ?
									activeQuestion ?
										<Question
											feedback={settings.knowledgeCheck ? questions[currentQuestion].feedback : feedback}
											answerQuestion={settings.knowledgeCheck ? false : this.answerQuestion.bind(this)}
											questionData={questions[currentQuestion]}
											settings={settings}
											correctness={settings.knowledgeCheck ? null : correctness}
											selection={userData[currentQuestion] ? userData[currentQuestion].selection : null}
											enableNext={this.enableNext}
											isLastQuestion={currentQuestion == questions.length-1 ? true : false}
											threshold={ threshold }
											getNextThreshold = { this.nextThreshold }
											style = { settings.style }
											isAssessment = { true } />
									:
									 <AssessmentReview completeAssessment={this.completeAssessment.bind(this)} scormInts={scormInts} reviewQuestion={this.reviewQuestion.bind(this)} />
								: assessmentComplete && postAssessment ?
										<AssessmentResults score={score} postAssessment={postAssessment} retry={this.retry.bind(this)} exit={this.exit.bind(this)} missedQuestions={missedQuestions} />
								:
									<Question
										feedback={settings.knowledgeCheck ? questions[currentQuestion].feedback : feedback}
										answerQuestion={settings.knowledgeCheck ? false : this.answerQuestion.bind(this)}
										questionData={questions[currentQuestion]}
										settings={settings}
										correctness={settings.knowledgeCheck ? null : correctness}
										selection={userData[currentQuestion] ? userData[currentQuestion].selection : null}
										enableNext={this.enableNext}
										isLastQuestion={currentQuestion == questions.length-1 ? true : false}
										threshold={ threshold }
										getNextThreshold = { this.nextThreshold }
										style = { settings.style }
										isAssessment = { true } />

							}

							{ assessmentStarted && !assessmentComplete && !assessmentReview || activeQuestion ?

								currentQuestion == questions.length - 1 && postAssessment == false && settings.knowledgeCheck || !this.state.showNext && settings.knowledgeCheck ? null :
								<Button disabled={ this.state.showNext && settings.knowledgeCheck ? false : !feedback && !activeQuestion ? true : false} onClick={this.nextQuestion} color={"primary"} variant={"raised"} > { activeQuestion ? "BACK TO REVIEW" : currentQuestion == questions.length - 1 ? "Results" : "Next Question" } </Button>
							: null }
						</div>
					</div>
					{settings.knowledgeCheck && thresholdDirections && !nextThresholdLoaded ?
						<InteractionBumper style={{ background: "#c74c4c", color: "#fff" }} text={ thresholdDirections } />
					: null}
				</div>
			:
				<p>Loading</p>
		)
	}
}

export default withTheme()(Assessment);
