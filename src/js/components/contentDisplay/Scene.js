const React = require('react')
import { withTheme } from '@material-ui/core/styles'

import InformationalPane from "./InformationalPane"
import ClickandDiscover from "./ClickandDiscover"
import DragNDrop from "./DragNDrop"
import Assessment from "./Assessment"
import CaseStudies from "./CaseStudies"
import Brochure from "./Brochure"
import Captivate from "./Captivate"
import Question from "./Question"
import Simulation from "./Simulation"
import Carousel from "./Carousel"
import NarrationPane from "./NarrationPane"

class Scene extends React.Component {
	render() {
		const { child, id } = this.props
		const styles = {
			container: {
				position: 'absolute',
				width: '100%',
				height: '100%',
				opacity: 0,
			}
		}

		const components = {
		    InformationalPane: InformationalPane,
		    ClickandDiscover: ClickandDiscover,
		    DragNDrop: DragNDrop,
		    Assessment: Assessment,
		    CaseStudies: CaseStudies,
		    Brochure: Brochure,
		    Captivate: Captivate,
		    Question: Question,
		    Carousel: Carousel,
		    NarrationPane: NarrationPane,
		    Simulation: Simulation
		}

		const ComponentType = components[Object.keys(child)[0]]
		const component = <ComponentType muiTheme={this.props.muiTheme} { ...Object.values(child)[0] } />

		return <div id={ id } style={ styles.container }>{ component }</div>
	}
}

export default withTheme()(Scene);
