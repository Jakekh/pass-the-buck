import React from 'react'

import ReactHtmlParser from "react-html-parser"

import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon';

const styles = theme => ({
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    CardContent: {
        position: 'relative',
    },
    CardActions: {
        marginTop: 'auto',
    },
    heading: {
        fontSize: 25,
        fontWeight: 100
    }
})

class CardComp extends React.Component {

    render() {
        const { classes, data, style } = this.props

        return (
        	<Card class={ classes.card } style={ data.style }>
                <CardContent class={ classes.CardContent }>
                    <Typography style={ {color: data.style.color} } variant="headline" component="div">{ ReactHtmlParser(data.title) }</Typography>
                    <Typography style={ {color: data.style.color, fontSize: 18} } component="div">{ ReactHtmlParser(data.text) }</Typography>
                    {
                        _.map(data.buttons, (button) => {
                            return <Button style={{marginBottom: "10px"}} fullWidth variant="raised" target="_blank" href={button.url}>{button.label}</Button>
                        })
                    }
                </CardContent>

            </Card>
        )
    }
}

CardComp.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(CardComp)
