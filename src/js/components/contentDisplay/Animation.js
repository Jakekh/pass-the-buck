var React = require('react')

import ReactHtmlParser from 'react-html-parser'

import {TweenMax, TimelineLite} from "gsap/TweenMax";

import { withTheme } from '@material-ui/core/styles'
import ButtonBase from '@material-ui/core/ButtonBase'
import Icon from '@material-ui/core/Icon'

import Stage from './Stage'
import Scene from './Scene'
import Timeline from './Timeline'

import * as CourseActions from '../../actions/CourseActions'
import LmsStore from '../../stores/LmsStore'
import ModalStore from '../../stores/ModalStore'

class Animation extends React.Component {
	constructor(props){
		super(props)
		const savedState = LmsStore.returnInteractionData(props.componentId)
		if(savedState != undefined){
			this.state = { completed: savedState.c, paused: true }
		}else{
			this.state = { completed: false, paused: true }
		}
	}

	componentDidMount () {
		const { elements, componentId } = this.props

		let targets = []
		let scenes = []

		_.map(elements, (value, key) => {
			const elementId = `animate_${componentId}_${key}`
			targets.push(elementId)
			scenes.push(<Scene key={ elementId } child={ value } id={ elementId } />)
		})

		this.setState({ targets, scenes })
	}

	reset = () => {
		this.setState({ paused: true })
	}

	initializeAnimation = () => {
		const { track } = this.props
		const { targets } = this.state
		const timelineObj = new TimelineLite({ paused: true })

		_.map(track.timing, ({ show, hide, time, speed }, timingKey) => {
			if(hide && show){
				_.map(hide, (value, key) => {
					timelineObj.to(`#${targets[value.target]}`, speed, value.effect, time)
				})
				_.map(show, (value, key) => {
					timelineObj.to(`#${targets[value.target]}`, speed, value.effect, time + speed)
				})	
			}

			if(hide && !show){
				_.map(hide, (value, key) => {
					timelineObj.to(`#${targets[value.target]}`, speed, value.effect, time)
				})
			}

			if(!hide && show){
				_.map(show, (value, key) => {
					timelineObj.to(`#${targets[value.target]}`, speed, value.effect, time)
				})
			}
		})

		this.setState({ tl: timelineObj })
	}

	complete = () => {
		const { getNextThreshold } = this.props
		const { componentId, completed } = this.state
		this.setState({ completed: true })
		CourseActions.saveInteraction({id: componentId, c: completed})
		getNextThreshold && getNextThreshold()
	}

	pausePlay = () => {
		const { paused, tl } = this.state

		paused ? tl.play() : tl.pause()
		this.setState({ paused: !paused })
	}

	seek = (pos) => {
		const { tl, paused } = this.state
		tl.seek(pos)
	}

	showAnimation = () => {
		const { getNextThreshold, componentId } = this.props

		const AnimationEls = <div>
			<Stage callback={ this.initializeAnimation } scenes={ this.state.scenes } />
			<Timeline pausePlay={ this.pausePlay } seek={ this.seek } audio={ this.props.track.audio } complete={ this.complete } reset={ this.reset } />
		</div>

		if(this.state.completed) {
			getNextThreshold && getNextThreshold()
		}

		ModalStore.createModal({ content: AnimationEls, onCloseEvent: 'animation_closed' })
	}

	render() {
		const { componentId, text, button } = this.props

		const styles = {
			animation: {
				background: '#0d1017',
				width: '100%',
				textAlign: 'center',
				paddingBottom: 170,
			},
			text: {
				font: 'normal 1.6rem/1.5 "Roboto", Arial, sans-serif',
			    marginTop: '-.3em',
			    marginBottom: '1.5em',
				color: '#fff',
				width: '100%',
			    padding: '5rem 4rem .8rem',
			    letterSpacing: '.15rem',
			},
			boldText: {
				font: 'normal 1.5rem/1.5 "Teko", Arial, sans-serif',
			    marginTop: '-.3em',
			    marginBottom: '1.5em',
				color: '#fff',
				width: '100%',
			    padding: '5rem 4rem .8rem',
			    letterSpacing: '.35rem',
			    textTransform: 'uppercase',
			},
			button: {
				color: '#fff',
			    border: '1px solid #fff',
			    textTransform: 'uppercase',
			    font: '1.3rem/2.9rem "Rajdhani", Arial, sans-serif',
			    fontSize: '.9rem',
			    letterSpacing: '.09em',
		        margin: '0 auto',
			    padding: '0 2.6rem',
		        fontSize: '1.3rem',
		        fontWeight: 700
			},
		}

		return (
			<div style={ styles.animation } class='mdl-grid' key={ componentId }>
				{ text && <p style={ styles.text }>{ text }</p> }
				{ button && <ButtonBase style={ styles.button } onClick={ this.showAnimation }>{ button }</ButtonBase> }
			</div>
		)	
	}
}
export default withTheme()(Animation)