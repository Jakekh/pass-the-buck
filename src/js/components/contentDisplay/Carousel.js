import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import MobileStepper from '@material-ui/core/MobileStepper'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon'
import Section from '../UI/Section'

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'
import CourseStore from '../../stores/CourseStore'

const styles = {
  
}

class Carousel extends React.Component {
  state = {
    activeStep: 0,
    complete: false,
    breakpoint: CourseStore.breakpoint
  }
  componentWillMount = () => {
    CourseStore.on( "breakpoint_changed", this.update_width )
  }
  componentWillUnmount = () => {
    CourseStore.removeListener( "breakpoint_changed", this.update_width )
  }
  update_width = () => {
    this.setState({ breakpoint: CourseStore.breakpoint })
  }
  handleNext = () => {
    let that = this
    const { breakpoint } = this.state
    this.setState({ activeStep: this.state.activeStep + 1, next: true }, () => {
    	this.checkComplete(),
      breakpoint.index <= 2 ? setTimeout( function() { document.getElementById(that.props.parentId).scrollIntoView(); window.scrollBy(0, -50);   }, 100 ) : null
    })
  }

  handleBack = () => {
    let that = this
    const { breakpoint } = this.state
    this.setState({ activeStep: this.state.activeStep - 1, next: false}, function() {
      breakpoint.index <= 2 ? setTimeout( function() { document.getElementById(that.props.parentId).scrollIntoView(); window.scrollBy(0, -50);  }, 100 ) : null
    })
  }

  checkComplete = () => {
  	const { activeStep } = this.state
  	const { panels } = this.props
  	activeStep == panels.length - 1 && this.setComplete()
  }

  setComplete = () => {
  	const { getNextThreshold } = this.props
  	!this.state.complete && getNextThreshold && getNextThreshold()
  	this.setState({ complete: true })
  }

  render() {
    const { classes, theme, panels } = this.props
    const { breakpoint } = this.state

  	const styles = {
  		nav: {
  			background: "#fff",
  		},
  		wbt_background: {
  			position: "relative",
  			overflow: "hidden",
  		},
  		panelHolder: {
				position: breakpoint.index > 2 ? 'absolute' : "relative",
        top: 0,
        left: 0,
				width: "100%",
  		},
      height_control: {
        height: breakpoint.index > 2 ? "700px" : "auto",
        overflow: breakpoint.index > 2 ? "hidden" : "auto",
      }
  	}

    let activePanel = panels[this.state.activeStep]
    return (
    	<div>
				<div style={{ ...styles.wbt_background, ...this.props.style[this.state.activeStep], ...styles.height_control}}>
					<div class="container">
						<div style={styles.panel}>
                <CSSTransitionGroup
			          transitionName={ breakpoint.index > 2 ? this.state.next ? "carouselPanel_NEXT" : "carouselPanel_BACK" : "none" }
			          transitionEnterTimeout={breakpoint.index > 2 ? 800 : 1}
			          transitionLeaveTimeout={breakpoint.index > 2 ? 800 : 1}>
									<div key={`panel_${this.state.activeStep}`} style={styles.panelHolder}>
										<Section children={Array.isArray(activePanel) ? activePanel : [ activePanel ]}  hideTitle={true} sectionId={`carousel_${this.props.componentId}_${this.state.activeStep}`} title={`carouselPanel_${this.state.activeStep}`} />
									</div>
								</CSSTransitionGroup>
						</div>
		      </div>
	      </div>
	      <div class="container" >
		      <MobileStepper
				      	style={styles.nav}
				        variant="dots"
				        steps={panels.length}
				        position="static"
				        activeStep={this.state.activeStep}
				        className={classes.root}
				        nextButton={
				           breakpoint.index > 2 ? 
                    <Button size="large" variant="raised" color="primary" onClick={this.handleNext} disabled={this.state.activeStep === panels.length - 1}>
  				            Next
  				            {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_left</Icon> : <Icon class="material-icons">chevron_right</Icon>}
				            </Button> 
                  :
                    <IconButton onClick={this.handleNext} disabled={this.state.activeStep === panels.length - 1} >
                      {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_left</Icon> : <Icon class="material-icons">chevron_right</Icon>}
                    </IconButton>

				        }
				        backButton={
                  breakpoint.index > 2 ? 
  				          <Button size="large" variant="raised" color="primary" onClick={this.handleBack} disabled={this.state.activeStep === 0}>
  				            {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_right</Icon> : <Icon class="material-icons">chevron_left</Icon>}
  				            Back
  				          </Button>
                  :
                    <IconButton onClick={this.handleBack} disabled={this.state.activeStep === 0} >
                     {theme.direction === 'rtl' ? <Icon class="material-icons">chevron_right</Icon> : <Icon class="material-icons">chevron_left</Icon>}
                    </IconButton>
				        }
				      />
	      </div>
      </div>
    )
  }
}

Carousel.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(Carousel)