import React from 'react'
import $ from 'jquery'

import ReactHtmlParser from "react-html-parser"

import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon';

import TextField from '@material-ui/core/TextField';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

import 'animation.gsap'
import 'debug.addIndicators'
import 'TextPlugin'
var ScrollMagic = require('scrollmagic')
import {TweenMax, TimelineLite, Animation} from "gsap";

import * as d3 from "d3";

const styles = theme => ({
    card: {
      height: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    CardContent: {
      position: 'relative',
    },
    CardActions: {
      marginTop: 'auto',
    },
    heading: {
      fontSize: 25,
      fontWeight: 100
    },
    bootstrapRoot: {
      padding: 0,
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    bootstrapInput: {
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 12px',
      width: 'calc(100% - 24px)',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.1)',
      },
    },
    bootstrapFormLabel: {
      fontSize: 18,
    },
    error: {
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      border: '1px solid #f00',
      fontSize: 16,
      padding: '10px 12px',
      width: 'calc(100% - 24px)',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.1)',
      },
    }
})

class Simulation extends React.Component {
    constructor(props){
        super(props)
        this.textInput = React.createRef()
        this.state = {
            failedSteps: 0,
            currentStep: 0,
            currentStepData: props.steps[0],
            modalOpen: false,
            dialogContent: null,
            dialogActions: null,
            stepTries: 0,
            simComplete: false,
            inputFeedback: null,
        }
    }

    componentDidMount = () => {
      const { componentId } = this.props
      let that = this
      d3.svg(`./${this.props.settings.svg.url}`).then( function(svg){

        let svgNode = svg.getElementsByTagName('svg')[0]
        console.log("APPENDING SVG: ", componentId, svgNode)
        $(`#svgHold_${componentId}`).append(svgNode)
        that.initSim()
      })
    }

    achieve = () => {
      const { currentStep } = this.state
      d3.selectAll('.hint').remove()
      console.log("Goal Acheived, next Goal...", currentStep, this.props.steps.length -1)
      if(currentStep < this.props.steps.length -1 ){
        this.setState({
            currentStep: this.state.currentStep + 1,
            currentStepData: this.props.steps[ this.state.currentStep + 1 ],
            stepTries: 0
        }, () => {
            this.initSim()
            console.log("NextStep: ", this.state.currentStep)
        })
      }else{
        console.log("SIM COMPLETE")
        if(this.state.failedSteps < 2){
          this.props.answerQuestion ? this.props.answerQuestion(true, null, "Sim Completed", this.props.questionData) : null
        }
        if(this.props.enableNext){
          this.props.enableNext()
        }
        this.setState({
          stepTries: 0,
          simComplete: true,
          failedSteps: 0,
        })
      }
    }

    remediate = () => {
      const { currentStep, currentStepData, stepTries, failedSteps } = this.state
      console.log(stepTries)
      let feedback = currentStepData.goal.feedback[this.state.stepTries]
      if(!feedback){
        feedback = currentStepData.goal.feedback[currentStepData.goal.feedback.length - 1]
      }
      console.log("REMEDIATORS...")
      let d3svg = d3.select(`#svgHold_${this.props.componentId}`).select('svg')
      d3.selectAll('.hint').remove()

      switch(currentStepData.goal.type){
        case "click":
          const button = d3svg.select(`#${currentStepData.goal.button.id}`)
          console.log(button)
          switch(feedback.type){
            case "glow" :
              var rect = d3svg.append("rect")
                .attr("x", button.attr("x"))
                .attr("y", button.attr("y"))
                .attr("height", button.attr("height"))
                .attr("width", button.attr("width"))
                .attr("stroke-width", 3)
                .attr("class", "hint")
                .style("stroke", "#f00")
                .style("fill", "none")

              let tl = new TimelineMax({repeat:-1, repeatDelay:0.5});
              tl.fromTo(".hint", .3,
                    {transformOrigin:"center center", autoAlpha:0 },
                    {transformOrigin:"center center", autoAlpha:1, ease:Quad.easeInOut}
                   )
                .fromTo(".hint", .3,
                    {transformOrigin:"center center", autoAlpha:1 },
                    {transformOrigin:"center center", autoAlpha:0, ease:Quad.easeInOut}
                   );
              this.setState({
                stepTries: this.state.stepTries + 1,
                failedSteps: this.state.stepTries + 1 == 2 ? this.state.failedSteps + 1 : this.state.failedSteps
              }, () => {
                console.log("FAILED STEPS: ", this.state.failedSteps)
                if(this.state.failedSteps >= 2){
                  this.props.answerQuestion ? this.props.answerQuestion(false, null, "Sim Completed", this.props.questionData) : null
                }
              })

            break
            case "text" :
              let dialogContent = <DialogContent>
                <DialogContentText id="alert-dialog-description">
                   { ReactHtmlParser(feedback.text) }
                </DialogContentText>
              </DialogContent>
              this.setState({
                modalOpen: true,
                dialogContent: dialogContent,
                dialogActions: this.getFeedbackActions(),
                stepTries: this.state.stepTries + 1,
                failedSteps: this.state.stepTries + 1 == 2 ? this.state.failedSteps + 1 : this.state.failedSteps
              }, () => {
                console.log("FAILED STEPS: ", this.state.failedSteps)
                if(this.state.failedSteps >= 2){
                  this.props.answerQuestion ? this.props.answerQuestion(false, null, "Sim Completed", this.props.questionData) : null
                }
              })
            break
          }
        break
        case "text-input" :
          let inputFeedback = feedback.text
          this.setState({
            inputFeedback:  <DialogContent>
                              <DialogContentText id="alert-dialog-description">
                                 { inputFeedback }
                              </DialogContentText>
                            </DialogContent>
          }, () => {
            this.setState({
               dialogContent: this.getTextField(),
               stepTries: this.state.stepTries + 1,
               failedSteps: this.state.stepTries + 1 >= 2 ? this.state.failedSteps + 1 : this.state.failedSteps
            }, () => {
              console.log("FAILED STEPS: ", this.state.failedSteps)
              if(this.state.failedSteps >= 2){
                this.props.answerQuestion ? this.props.answerQuestion(false, null, "Sim Completed", this.props.questionData) : null
              }
            })
          })
        break
      }
    }

    /*handleInputChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };*/

    submitStep = () => {
      const { currentStepData, stepInput, modalOpen  } = this.state
      let correctness = false
      console.log("REF: ", this.textInput.current.value)
      _.map(currentStepData.goal.answers, (answer) => {
        if(this.textInput.current.value.toString() == answer){
          correctness = true
        }
      })
      if(correctness){
        if(modalOpen){
          this.setState({
            modalOpen: false,
            inputFeedback: null
          }, () => {
            this.achieve()
          })
        }
      }else{
        this.remediate()
      }
    }

    getTextField = () => {
        const { classes } = this.props
        const { currentStepData, inputFeedback } = this.state
        console.log("INPUT FB: ", inputFeedback )
        return <DialogContent>
                { currentStepData.goal.modalTextInput.modalContent && <DialogContentText id="alert-dialog-description">
                   { ReactHtmlParser(currentStepData.goal.modalTextInput.modalContent) }
                </DialogContentText> }
                 <TextField
                  onKeyPress={(ev) => {
                    console.log(`Pressed keyCode ${ev.key}`);
                    if (ev.key === 'Enter') {
                      // Do code here
                      ev.preventDefault();
                      this.submitStep()
                    }
                  }}
                  inputRef={this.textInput}
                  //value={this.state.stepInput }
                  //onChange={ this.handleInputChange('stepInput')}
                  placeholder={ !this.state.stepInput ? currentStepData.goal.modalTextInput.defaultValue ? currentStepData.goal.modalTextInput.defaultValue : null : null }
                  label={ !this.state.stepInput ? currentStepData.goal.modalTextInput.label ? currentStepData.goal.modalTextInput.label : null : null }
                  id="stepInput"
                  InputProps={{
                    disableUnderline: true,
                    classes: {
                      root: classes.bootstrapRoot,
                      input: inputFeedback ? classes.error : classes.bootstrapInput
                    },
                  }}
                  InputLabelProps={{
                    shrink: true,
                    className: classes.bootstrapFormLabel,
                  }}
                />
              </DialogContent>
    }
    getTextFieldActions = () => {
      return  <DialogActions>
                <Button onClick={ this.submitStep } color="primary">
                    Submit
                </Button>
              </DialogActions>
    }
    getFeedbackActions = () => {
      return  <DialogActions>
                <Button onClick={ this.closeModal } color="primary">
                    Close
                </Button>
              </DialogActions>
    }
    initSim = () => {
      console.log("INIT SIM")
      const { componentId } = this.props
      const { currentStep, currentStepData, simComplete } = this.state
      let el = $(`#svgHold_${componentId}`).find('svg')[0]
      //for current step visually configure svg & assign click events
      if(currentStepData.goal){
        switch(currentStepData.goal.type){
          case "click" :
              const button = $(el).find(`#${currentStepData.goal.button.id}`)
              $(button).css('cursor', 'pointer')
              $(button).on('click', () => {
                 this.achieve()
              })
              //incorrect click
              if(currentStepData.goal.incorrect) {
                const incorrectEl = $(el).find(`#${currentStepData.goal.incorrect}`)
                $(incorrectEl).css('cursor', 'pointer')
                $(incorrectEl).off().on('click', () => {
                  console.log(this.state.simComplete)
                  !this.state.simComplete ? this.remediate() : null
                })
              }
          break
          case "text-input" :
              this.setState({
                  modalOpen: true,
                  dialogContent: this.getTextField(),
                  dialogActions: this.getTextFieldActions(),
              })
          break
        }
      }else{
        //No goal here, so were moving on or viewing final Feedback.
        this.achieve()
      }
      //Visible Classes
      _.map(currentStepData.config.visibleClasses, (eclass) => {
          $(el).find(`.${eclass}`).css({display: "block"})
      })
      //Hidden Classes
      _.map(currentStepData.config.hiddenClasses, (eclass) => {
          $(el).find(`.${eclass}`).css({display: "none"})
      })
      //Visible Elements
      _.map(currentStepData.config.visibleElements, (id) => {
          $(el).find(`#${id}`).css({display: "block"})
      })
      //Hidden Elements
      _.map(currentStepData.config.hiddenElements, (id) => {
          $(el).find(`#${id}`).css({display: "none"})
      })

    }

    closeModal = () => {
        this.setState({
            modalOpen: false
        })
    }

    render() {
        const { modalOpen, dialogContent, dialogActions, inputFeedback, failedSteps } = this.state
        const { classes, settings, steps, componentId } = this.props
        //<object id={`sim_svg_${componentId}`} type="image/svg+xml" data={ settings.svg.url } />
        const styles={
          failedMsg: {
            background: "#bb3131",
            color: "#fff",
            padding: 15,
            borderRadius: 5,
            margin: "20px 0",
            fontWeight: "bold"
          }
        }
        return (
            <div class="container">
                <div style={{marginBottom: 30}}>
                    { this.props.directions && <h5 style={this.props.directions.style}>{ ReactHtmlParser(this.props.directions.text) }</h5> }
                    <div id={`svgHold_${componentId}`} class="svg-sim"></div>
                    { failedSteps >= 2 ? <div style={styles.failedMsg}>{this.props.settings.failedFeedback}</div> : null }
                </div>
                <Dialog onClose={this.closeModal} aria-labelledby="simple-dialog-title" open={modalOpen} disableBackdropClick={true} fullWidth maxWidth="md">
                    { dialogContent }
                    { inputFeedback }
                    { dialogActions }
                </Dialog>
            </div>
        )
    }
}

Simulation.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(Simulation)
