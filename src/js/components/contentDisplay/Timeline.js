var React = require('react')
import ReactHowler from 'react-howler'
import raf from 'raf' // requestAnimationFrame polyfill
import { withStyles } from '@material-ui/core/styles'

import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Slider from '@material-ui/lab/Slider'
import MediaStore from '../../stores/MediaStore'
import ModalStore from '../../stores/ModalStore'

const styles = {
	thumb: {
		backgroundColor: '#fff',
	},
	trackBefore: {
		backgroundColor: '#fff',
	},
	trackAfter: {
		backgroundColor: '#fff',
		opacity: 0.24
	},
}

class Timeline extends React.Component {
	constructor (props) {
		super(props)

		this.state = {
			playing: false,
			loaded: false,
			loop: false,
			mute: false,
			volume: 1.0,
			done: false,
			sliderVal: 0
		}
	}

	componentWillUnmount () {
		this.clearRAF()
		MediaStore.removeListener('stop_media', this.checkStop)
		ModalStore.removeListener('animation_closed', this.reset)
	}

	componentDidMount () {
		MediaStore.on('stop_media', this.checkStop)
		ModalStore.on('animation_closed', this.reset)
	}

	reset = () => {
		this.setState({
			playing: false,
			loaded: false,
			done: false
		})
		this.props.reset() // -- animation.reset()
	}

	checkStop = () => {
		const { playing } = this.state
		playing && this.handleStop()
	}

	handleToggle = () => {
		const { playing } = this.state
		MediaStore.stopMedia()
		this.setState({ playing: !playing })
		this.props.pausePlay() // -- animation.pausePlay()
	}

	handleOnLoad = () => {
		this.setState({
			loaded: true,
			duration: this.player.duration()
		})
	}

	handleOnPlay = () => {
		this.setState({ playing: true })
		this.renderSeekPos()
	}

	handleOnEnd = () => {
		this.setState({ playing: false, done: true })
		this.clearRAF()
		this.props.complete()
		this.props.pausePlay()
	}

	handleStop = () => {
		if(this.player){
			this.player.pause()
			this.setState({ playing: false })
			this.renderSeekPos()
		}
	}

	handleLoopToggle = () => {
		const { loop } = this.state
		this.setState({ loop: !loop })
	}

	handleMuteToggle = () => {
		const { mute } = this.state
		this.setState({ mute: !mute })
	}

	renderSeekPos = () => {
		const { playing, duration } = this.state
		this.setState({ seek: this.player.seek(), sliderVal: this.player.seek() / duration * 100 })// -- this is wrong
		if(playing) {
			this._raf = raf(this.renderSeekPos)
		}
	}

	replay = () => {
		this.player.seek(0)
		this.setState({ seek: this.player.seek() })
		this.props.seek(0) // -- animation.seek()
		this.reset()
		this.handleToggle()
	}

	handleChange = (event, sliderVal) => {
	    this.setState({ sliderVal })// -- this is wrong too
	    this.seekAudio()
	}

	seekAudio = () => {
		const { duration, sliderVal } = this.state
		this.player.seek(duration * sliderVal/100)
		this.setState({ seek: this.player.seek() })
		this.props.seek(duration * sliderVal/100) // -- animation.seek()
		this.setState({ done: false })
	}

	clearRAF = () => {
		raf.cancel(this._raf)
	}

	dragStart = () => {
		const { playing } = this.state
		this.setState({ volume: 0, buffering: playing })
		playing && this.handleToggle()
	}

	dragEnd = () => {
		const { buffering } = this.state
		buffering && this.handleToggle()
		this.setState({ volume: 1.0 })
	}

	render() {
		const { sliderVal, done, playing, loop, mute, volume } = this.state
		const { audio, classes } = this.props

		const styles = {
			playBtn:{
				color: "#fff"
			},
			seekbar:{
				flex: 1,
				paddingLeft: 10,
				alignSelf: "center",
			},
			timeline: {
				position: "absolute",
				bottom: 0,
				width: "100%",
				padding: 10,
				boxSizing: "border-box", 
				backgroundColor: "#0d1017",
				display: 'flex',
			}
		}

		return (
			<div class='full-control' style={ styles.timeline }>
				<ReactHowler
				src={ audio }
				playing={ playing }
				onLoad={ this.handleOnLoad }
				onPlay={ this.handleOnPlay }
				onEnd={ this.handleOnEnd }
				loop={ loop }
				mute={ mute }
				volume={ volume }
				ref={ (ref) => (this.player = ref) } />

				<div style={ styles.playBtn}>
					<IconButton onClick={ done ? this.replay : this.handleToggle } style={ styles.playBtn }>
						{ done ? <Icon>replay</Icon> : this.state.playing ? <Icon>pause</Icon> : <Icon>play_arrow</Icon> }
					</IconButton>
				</div>
				<div style={ styles.seekbar }>
					<Slider classes={{ thumb: classes.thumb, trackBefore: classes.trackBefore, trackAfter: classes.trackAfter }} onChange={ this.handleChange } onDragStart={ this.dragStart } onDragEnd={ this.dragEnd } variant="determinate" value={ sliderVal } />
				</div>
			</div>
		)	
	}
}

export default withStyles(styles)(Timeline)