,
		{
			"title": "Assessment",
			"sections": [
				{
					"Banner": {
						"title": "Passenger Services - Space Required Travel",
						"subTitle": "FINAL ASSESSMENT",
						
						"image": "src/data/course-assets/badge_full.png",
						"parralaxLayers": [
							{
								"src": "src/data/course-assets/amwc_1.png",
								"direction": "from",
								"animation": {
									"scaleX": "1.1",
									"scaleY": "1.1",
									"transformOrigin": "center center",
									"ease": "Power4.easeOut"
								}
							},
							{
								"src": "src/data/course-assets/amwc_2.png",
								"direction": "to",
								"animation": {
									"left": "50",
									"ease": "Power4.easeOut"
								}
							}
						]
					}
				},
				{
					"Section": {
						"threshold": true,
						"title": "Assessment",
						"children":[
							{
								"Assessment":{
									"scored": true, 
									"masteryScore": 80,
									"settings": {
										"show_feedback": false,
										"review_before_submit": true,
										"style": {
											"paddingBottom": 50
										}
									},
									"intro": "Welcome to the Final Assessment. To get started, Select Begin.",
									"PostAssessment":{
										"media": {
											"type": "image",
											"url": "src/data/course-assets/caddy_2.png"
										},
										"text": "Assessment Complete!"
									},
									"questionPools":[
										{
											"pull": 3,
											"title": "Pool 1",
											"questions":[
												{
													"type": "cb",
													"directions": "Select all that apply. Select Submit Answers when done.",
													"questionText": "In which chapter of the T.O. 1C-130A-9 would you find vehicle overhang and projection limits?",
													"reviewContent": [ 0, 1 ],
													"assoc_img": "src/data/course-assets/Assessment/assoc_img.JPG",
													"distractors": [
														{
															"value": "Chapter 1- Introduction",
															"correct": false
														},
														{
															"value": "Chapter 2- Description of Aircraft Features",
															"correct": false
														},
														{
															"value": "Chapter 3- Aircraft Configuration (correct)",
															"correct": true
														},
														{
															"value": "Chapter 4- General Procedures (correct)",
															"correct": true
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "cb",
													"directions": "Select all that apply. Select Submit Answers when done.",
													"questionText": "What resource would be used to find descriptions of specific airframe features on the C-130?",
													"reviewContent": [ 0, 1 ],
													"assoc_img": "src/data/course-assets/Assessment/assoc_img.JPG",
													"distractors": [
														{
															"value": "AFI 11-2C-130 V3 Addenda A (correct)",
															"correct": true
														},
														{
															"value": "AFI 11-2C-130 V3",
															"correct": false
														},
														{
															"value": "T.O. 1C-130A-9 Ch3 (correct)",
															"correct": true
														},
														{
															"value": "T.O 1C-130E-5 Ch3e",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "Of the pallet positions given, Where would you place a pallet with module type G (C-130H propeller with maximum height of 102\" on one side) on a C-130E?",
													"reviewContent": [ 0, 2 ],
													"distractors": [
														{
															"value": "PP 6 (correct)",
															"correct": true
														},
														{
															"value": "PP 4",
															"correct": false
														},
														{
															"value": "PP 3",
															"correct": false
														},
														{
															"value": "PP 1",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "On a C-130H with the forward racks installed, where would you place a module type Y, 3 pallet train - 78 inches tall, 6 inch aisle-way, and no cut-out on 88\" side?",
													"reviewContent": [ 0, 2 ],
													"distractors": [
														{
															"value": "Pallet Positions 1-3",
															"correct": false
														},
														{
															"value": "Pallet Positions 2-4 (correct)",
															"correct": true
														},
														{
															"value": "Pallet Positions 3-6",
															"correct": false
														},
														{
															"value": "Both A and B",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												}
											]
										},
										{
											"pull": 2,
											"title": "Pool 2",
											"questions":[
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "Cargo to be loaded on a C-130 weighs 18,500 lbs and contains two skids, each 75 inches long. What is the roller station load for this cargo?",
													"reviewContent": [ 0, 2 ],
													"distractors": [
														{
															"value": "1,028 lb/ roller station",
															"correct": false
														},
														{
															"value": "1,321 lb/ roller station (correct)",
															"correct": true
														},
														{
															"value": "2,643 lb/ roller stationh",
															"correct": false
														},
														{
															"value": "9,250 lb/ roller station",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "Select the best answer using Figure 4A-14 in the T.O. -9. What is the rolling and parking shoring thickness required for a forklift with hard rubber tires to be loaded on a C-130? The forklift axle weight is 2,300 lbs, 2 wheels per axle, 4-inch wheel width.",
													"reviewContent": [ 0, 2 ],
													"distractors": [
														{
															"value": "Rolling 0.50/ Parking 1.00",
															"correct": false
														},
														{
															"value": "Rolling 0.75/ Parking 1.50",
															"correct": false
														},
														{
															"value": "Rolling 1.00/ Parking 1.00",
															"correct": false
														},
														{
															"value": "Rolling 1.00/ Parking 2.00 (correct)",
															"correct": true
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "Select the best answer using Chart B from the T.O. -9. An F-16 engine on a dolly with 13\" ground clearance and 250\" wheelbase will be loaded on a C-130. What is the maximum ramp hinge height at which the vehicle will clear the ramp crest?",
													"reviewContent": [ 0, 2 ],
													"distractors": [
														{
															"value": "34",
															"correct": false
														},
														{
															"value": "36",
															"correct": false
														},
														{
															"value": "38 (correct)",
															"correct": true
														},
														{
															"value": "40",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												},
												{
													"type": "mc",
													"directions": "Select the best answer from the choices provided.",
													"questionText": "Select the best answer using Chart C from the C-130 T.O. -9. A Stakebed truck with ground clearance of 16\" and overhang of 40\" will be driven onto a C-130. What is the farthest aft fuselage station the rear axle should be parked?",
													"reviewContent": [ 0, 1 ],
													"distractors": [
														{
															"value": "742 (correct)",
															"correct": true
														},
														{
															"value": "730",
															"correct": false
														},
														{
															"value": "721",
															"correct": false
														},
														{
															"value": "708",
															"correct": false
														}
													],
													 "feedback": {
												        "correct": {
												            "value": "That’s the best news I’ve heard all day!"
												        },
												        "incorrect": {
												            "value": "That is fairly common, but have you considered thinking about pricing at the individual tee time level? Can you look back at your historical data and find individual tee times that you know have had more demand than others? Could you price them differently and grow your revenue? If you need a way to make more money off the same rounds of golf, dynamic pricing is the way to do it."
												        },
												        "tryagain": {
												            "value": "Not quite. Try again."
												        }
												    }
												}
											]
										}
									]
								}
							}
						]
					}
				},
				{
					"Footer": {
						"threshold": true,
						"title": "Footer",
						"directions": "From here you can restart this chapter or continue to the next chapter."
					}
				}
			]
		}