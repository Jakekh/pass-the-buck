// to set the NODE_ENV to production use -- set NODE_ENV=production then run webpack
// this minifies/uglifis the script

var mode = "development";
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require("path")

module.exports = {
    mode: mode,
    context: __dirname,
    devtool: mode == "development" ? "inline-sourcemap" : false,
    entry: {
        app: __dirname +"/src/js/scripts.js",
        vendor: ["jquery", "lodash", "react", "material-design-lite", "react-html-parser", "react-dom"]
    },
    module: {
        rules: [
            {
                test: /\.node$/,
                loader: "node-loader"
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            }
        ]
    },
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        dns: 'mock',
        net: 'mock'
    },
    resolve: {
      extensions: [".webpack.js", ".web.js", ".js", ".node"],
      alias: {
        "TweenLite": path.resolve('node_modules', 'gsap/src/uncompressed/TweenLite.js'),
        "TweenMax": path.resolve('node_modules', 'gsap/src/uncompressed/TweenMax.js'),
        "TimelineLite": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineLite.js'),
        "TimelineMax": path.resolve('node_modules', 'gsap/src/uncompressed/TimelineMax.js'),
        "TextPlugin": path.resolve('node_modules', 'gsap/src/uncompressed/plugins/TextPlugin.js'),

        "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
        "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
        "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js')
      }
    },
    optimization: {
      splitChunks: {
        chunks: 'async',
        minSize: 30000,
        maxSize: 0,
        minChunks: 2,
        maxAsyncRequests: 5,
        maxInitialRequests: 3,
        automaticNameDelimiter: '_',
        name: true,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js',
        sourceMapFilename: '[name].map',
        chunkFilename: '[id].js'
    },
    plugins: mode == "development" ? [
        //new webpack.optimize.CommonsChunkPlugin({name: "vendor", filename: "vendor.bundle.js"}),
        new CopyWebpackPlugin([
             { from: 'src/data/course-assets', to: 'src/data/course-assets' },
             { from: 'index.html' }
        ])
    ] : [
        new webpack.DefinePlugin({
            'process.env': {
              'NODE_ENV': JSON.stringify('production')
            }
          }),
        //new webpack.optimize.CommonsChunkPlugin({name: "vendor", filename: "vendor.bundle.js"}),
        // new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
        new CopyWebpackPlugin([
             { from: 'src/data/course-assets', to: 'src/data/course-assets' },
             { from: 'index.html' }
        ])
    ],
};
